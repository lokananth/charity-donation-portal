<div id="show_data_display" class="tab-content bg-color-white padding-10">
					<div class="tab-pane fade in active" id="s1">
						<br><div id="buttonPlaceholder" align="right"></div>
                        <?php  //print_r($list);?>
                        <form id="show_charity" action="docpost" name="show_charity" class="smart-form" novalidate="novalidate" method="post" enctype="multipart/form-data">
							  <legend>Generic Info</legend>  
                            <fieldset>
                                      <!-- First -->
													<div class="form-group">
														<div class="row">
															<div class="col-sm-12 col-md-6 has-feedback">
                                                             
																<label class="col-md-5 control-label">Organisation Name </label>
                                                                
																<?php echo $list[0]->Name; ?> 
															 </div>

															<div class="col-sm-12 col-md-6 has-feedback">
																<label class="col-md-5 control-label">Phone Number</label>
																<?php echo $list[0]->Phone_Number; ?> 
															 </div>

															 
														</div>
													</div><br />
										 <!-- Second -->	
                                         <div class="form-group">
														<div class="row">
															<div class="col-sm-12 col-md-6 has-feedback">
																<label class="col-md-5 control-label">Trustee </label>
																<?php echo $list[0]->Trustee; ?>
															 </div>

															<div class="col-sm-12 col-md-6 has-feedback">
																<label class="col-md-5 control-label">Registration Date</label>
																<?php echo $list[0]->Registration_Date; ?>
															 </div>

															 
														</div>
													</div><br />
										 <!-- Three -->	
                                          <div class="form-group">
														<div class="row">
															<div class="col-sm-12 col-md-6 has-feedback">
																<label class="col-md-5 control-label">Address </label>
																<?php echo $list[0]->Address; ?>
															 </div>

															<div class="col-sm-12 col-md-6 has-feedback">
																<label class="col-md-5 control-label">Type</label>
																<?php if($list[0]->Type==1) echo "Non Profit Organisation"; elseif($list[0]->Type==2) echo "Charity"; elseif($list[0]->Type==3) echo "Other";?>
															 </div>

															 
														</div>
													</div><br />
										 <!-- Four -->	
                                          <div class="form-group">
														<div class="row">
															<div class="col-sm-12 col-md-6 has-feedback">
																<label class="col-md-5 control-label">Contribute Percentage </label>
																<?php echo $list[0]->Contribution_Percent; ?>
															 </div>
 
														</div>
													</div>
										 <!-- Set -1 Over -->	
					 </fieldset>
					 <br />
			<!-- Finance -->			 			
							<legend>Finance Info</legend>  
									<fieldset> 
                                    <!-- One  -->	
                                          <div class="form-group">
														<div class="row">
															<div class="col-sm-12 col-md-6 has-feedback">
																<label class="col-md-5 control-label">Bank Name </label>
																<?php echo $list[0]->Bank_Name; ?>
															 </div>
  
														</div>
													</div><br />
										 <!-- Two -->	
                                          <div class="form-group">
											<div class="row">
                                        	 <div class="col-sm-12 col-md-6 has-feedback">
											 	<label class="col-md-5 control-label">Account Holder Name</label>
												<?php echo $list[0]->Account_Holder_Name; ?>
											 </div>
                                            </div>
										  </div><br />
                                           <!-- Three -->	
                                          <div class="form-group">
											<div class="row">
                                        	 <div class="col-sm-12 col-md-6 has-feedback">
											 	<label class="col-md-5 control-label">Sort Code</label>
												<?php echo $list[0]->Sort_Code; ?>
											 </div>
                                            </div>
										  </div><br />
										  <!-- Three -->	
                                          <div class="form-group">
											<div class="row">
                                        	 <div class="col-sm-12 col-md-6 has-feedback">
											 	<label class="col-md-5 control-label"> Account Number</label>
												<?php echo $list[0]->Account_Number; ?>
											 </div>
                                            </div>
										  </div><br />
									<!-- Set -2 Over -->	
					 </fieldset>
					 <br />
			<!-- Uploaded -->			 	
             						<legend>Charity Login Details</legend>    
                                    <fieldset >
                                     <div class="form-group">
														<div class="row">
															<div class="col-sm-12 col-md-6 has-feedback">
																<label class="col-md-5 control-label">User Name </label>
																<?php echo $list[0]->Username; ?>
															 </div>
  
														</div>
													</div><br />
                                       <div class="form-group">
														<div class="row">
															<div class="col-sm-12 col-md-6 has-feedback">
																<label class="col-md-5 control-label">Password </label>
																XXXXXX
															 </div>
  
														</div>
													</div><br />
                                         <div class="form-group">
														<div class="row">
															<div class="col-sm-12 col-md-6 has-feedback">
																<label class="col-md-5 control-label">Email Id </label>
																<?php echo $list[0]->Email; ?>
															 </div>
  
														</div>
													</div><br />
                                    </fieldset>
                     
                                    <legend>Uploaded Documents</legend>
									<fieldset> 
                                    <div id="fileinput0">
										<div class="row">
										<section class="col-sm-12 col-md-12 has-feedback"> 
                                        <div id="showuplodeddoc_none" style="display:none"></div>
                                       <?php 
									   $getfilename = $list[0]->Files_Name;
									   $getdoctype = $list[0]->Doc_Type;
									   $findme   = ',';
									   $pos = strpos($getfilename, $findme);
									   
									   if ($pos === false) {
										   $showFilename = $getfilename;
										   if($showFilename!='') {
										   echo '<div id="showdoctype_0" class="row"><div class="col col-3" id="showdoctype_0">Doc type </div>';
										   echo '<div class="col col-3">'.$showFilename.'</div>';
										   echo '<div class="col col-3"><a href="uploaded_file/'.$showFilename.'" download>Download</a> </div>';
										   echo '<div class="col col-3"><a id="delete_upload_0" href="'.$showFilename.','.$getdoctype.'">Delete</a></div></div>'; 											}
										   else
										   echo '<div id="showuplodeddoc_none">You dont have any uploaded doc!.</div>';
										} else {
											$getSeperateFiles = explode(',',$getfilename);
											$getSeperateDoctype = explode(',',$getdoctype);
											//echo count($getSeperateFiles);
											for($j=0;$j<count($getSeperateFiles);$j++)
											{
												//echo $getSeperateFiles[$j]; 
												echo '<div id="showdoctype_'.$j.'" class="row"><div class="col col-3">Doc type </div>';
											    echo '<div class="col col-3">'.$getSeperateFiles[$j].'</div>';
												 echo '<div class="col col-3"><a href="uploaded_file/'.$getSeperateFiles[$j].'" download>Download</a> </div>';
											
												echo '<div class="col col-3"><a id="delete_upload_'.$j.'" href="'.$getSeperateFiles[$j].','.$getSeperateDoctype[$j].'">Delete</a></div></div>';
												
												}
										}
									   ?>
										</section>
                                        </div>
                                      </div>
                                      <br />
									</fieldset>
                                   
                     <!-- Account Creation Details -->             
                                  <legend>Account Creation Details</legend>    
                                   <fieldset >
                                     <!-- one  -->	
                                     <div id="fileinput0">
                                          <div class="form-group">
														<div class="row">
															<div class="col-sm-12 col-md-6 has-feedback">
																<label class="col-md-5 control-label">Created By </label>
																<?php /*echo $list[0]->Created_By;*/ echo $this->session->userdata('username'); ?>
															 </div>

															<div class="col-sm-12 col-md-6 has-feedback">
																<label class="col-md-5 control-label">Modified By  </label>
																<?php echo $list[0]->Modified_By; ?>
															 </div>
 
														</div>
													</div><br />
										 <!-- two -->	
                                          <div class="form-group">
														<div class="row">
															<div class="col-sm-12 col-md-6 has-feedback">
																<label class="col-md-5 control-label">Created On </label>
																<?php /*echo $list[0]->Created_By;*/ echo $list[0]->Created_On; ?>
															 </div>

															<div class="col-sm-12 col-md-6 has-feedback">
																<label class="col-md-5 control-label">Last Modified Date </label>
																<?php echo $list[0]->Modified_Date; ?>
															 </div>
 
														</div>
													</div><br />
										 <!-- two -->	
                                </div>
                                     
									</fieldset>
								</form>
									
								<footer>
                                <button type="submit" class="btn btn-primary" id="edit_button">
											Edit
										</button>
    									<button type="button" class="btn btn-primary" id="edit_close">
											Close
										</button>
										
                                        </footer>
                                
                        
                        
                    </div>
		
										
					
				</div>
                
<!-- Edit Page -->

<div id="show_edit_form" class="tab-content bg-color-white padding-10">
					<div class="tab-pane fade in active" id="s1">
						<br><div id="buttonPlaceholder" align="right"></div>
                        <form id="edit_charity" action="docpost" name="edit_charity" class="smart-form" novalidate="novalidate" method="post" enctype="multipart/form-data">
<input type="hidden" name="vendor_id" id="vendor_id" value="<?php  echo $vendor_id;?>" />
									 <legend>Generic Info</legend>  
                            <fieldset>
                            <!-- First -->
								  
                                 <div class="form-group">
											<section class="col col-6 no-padding">Organisation Name
												<label class="input">
													<input readonly="readonly" type="text" name="org_name" id="org_name" placeholder="Enter Name" value="<?php echo $list[0]->Name; ?>">
												</label>
											</section>
											<section class="col col-6">Phone Number
												<label class="input"> 
												<input type="text" name="phone_number" id="phone_number" placeholder="Enter Number" value="<?php echo $list[0]->Phone_Number; ?>">
												</label>
											</section>
									 
                                          
								</div><br />
							 <!-- Second -->	
                             <div class="form-group">
											<section class="col col-6 no-padding">
												<label class="input"> Trustee
													<input type="text" name="trustee"  id="trustee" placeholder="Enter Name" value="<?php echo $list[0]->Trustee; ?>">
												</label>
											</section>
											<section class="col col-6">Registration Date
                                            <label class="input"> <i class="icon-append fa fa-calendar"></i>
													<input type="text" name="reg_date" id="reg_date" value="<?php echo $list[0]->Registration_Date; ?>" >
												</label>
											</section>
										</div>
                              <br />
                              <div class="form-group">
											<section class="col col-6 no-padding">
												<label class="input"> Address
												  <textarea rows="3" cols="37" name="address"  id="address" placeholder="Enter Address"><?php echo $list[0]->Address; ?></textarea> 
												</label>
											</section>
											<section class="col col-6">Type
												
											<div class="inline-group">
												<label class="radio">
													<input type="radio" name="org_type" <?php if($list[0]->Type==1) { ?> checked="checked"<?php } ?>value="1">
													<i></i>Non Profit Organisation</label> 
												<label class="radio">
													<input type="radio" name="org_type" <?php if($list[0]->Type==2) { ?> checked="checked"<?php } ?> value="2">
													<i></i>Charity</label>
												<label class="radio">
													<input type="radio" name="org_type" <?php if($list[0]->Type==3) { ?> checked="checked"<?php } ?> value="3">
													<i></i>Other</label>
											</div>
										</section>
										</div><br>
                                        <!-- Four -->	
                                	<div class="form-group">
										<section class="col col-6"> Percentage
										<label class="input">
										<input type="text" name="percentage" id="percentage" placeholder="Enter Number only" value="<?php echo $list[0]->Contribution_Percent; ?>"> 
										</label>
                                       </section>
                                    </div>
										  
										 <!-- Set -1 Over -->	
					 </fieldset>
					 
						 <legend>Finance  Info</legend>
									<fieldset>
                                    <div class="row">
										<section class="col col-8">Bank Name
										<label class="input">
										 <input type="text" name="bank_name" id="bank_name" placeholder="Enter Name" value="<?php echo $list[0]->Bank_Name; ?>">
										</label>
                                       </section>
                                    </div>
                                    <div class="row">
										<section class="col col-8"> Account Holder Name
										<label class="input">
										 <input type="text" name="acc_holder_name" id="acc_holder_name" placeholder="Enter Name" value="<?php echo $list[0]->Account_Holder_Name; ?>">
										</label>
                                       </section>
                                    </div>
										 
                                         <div class="row">
										<section class="col col-8">Sort Code
										<label class="input"> 
                                           <div class="col col-4 no-padding">
                                           <input type="text" name="sort_code1" id="sort_code1" maxlength="2" placeholder="" value="<?php echo substr($list[0]->Sort_Code,0,2); ?>">
                                            </div>										
										   <div class="col col-4">
			                               <input maxlength="2" type="text" name="sort_code2" id="sort_code2" placeholder="" value="<?php echo substr($list[0]->Sort_Code,2,2); ?>">					
                                           </div>				 
                                           <div class="col col-4 no-padding">
                                           <input type="text" name="sort_code3" id="sort_code3" placeholder="" maxlength="2" value="<?php echo substr($list[0]->Sort_Code,4,2); ?>">	
                                           </div>										
									   </label>
                                        </label>
                                       </section>
                                    </div>
										  <div class="row">
										<section class="col col-8"> Account Number
										<label class="input">
										<input type="text" name="acc_number" id="acc_number" placeholder="Enter Number" value="<?php echo $list[0]->Account_Number; ?>">
										</label>
                                       </section>
                                    </div>
										  
                                        
                                       
									</fieldset>
                                    <legend>Upload Documents</legend>
									<fieldset> 
                                    <div id="fileinput0">
										<div class="row">
                                            <section class="col col-5 row no-padding"> 
                                                <div class="col col-5"> Select doc type</div>
                                                <div  class="col col-7 row no-padding"><select name="doc_type0" class="form-control" id="doc_type0" class="hello">
														<option value="" >Select</option>
														<option value="ID Proof">ID Proof</option>
														<option value="Address Proof">Address Proof</option>
														<option value="Stamp Pager doc">Stamp Pager doc</option>
														<option value="Signed Authority Letter">Signed Authority Letter</option>
														<option value="Bank Letter">Bank Letter</option>
													</select> </div>
                                            </section>
                                            <section class="col col-4  row no-padding">  
                                                <div class="input input-file">
                                                <div class="col col-10"> <label class="input">
                                                        <input type="file" name="upload_0" id="upload_0"  class="ipfile"/>
                                                        
                                                    </label> </div>
                                                    <div  class="col col-2">
                                                    <div id="showdeleteupload_0" style="display:none">
                                                       <a id="delete_upload_0" href="javascript: void(0);">Delete</a></div>
                                                    </div>
                                                </div>  
                                            </section>
                                            <section class="col col-3"> 
                                                <div style="float:left">  | + <a href="javascript: void(0);" id="add_more_1" style="text-decoration:underline" >
                                                    <em>Add More</em></a></div> </div>
                                            </section> 
                                        </div>
                                     </div>
                                         
             
                                      <br />
                                      <div id="fileinput1" style="display:none">
										<div class="row">
										<section class="col col-6"> Select doc type
                                        <select name="doc_type1" id="doc_type1" class="hello">
														<option value="" >Select</option>
														<option value="ID Proof">ID Proof</option>
														<option value="Address Proof">Address Proof</option>
														<option value="Stamp Pager doc">Stamp Pager doc</option>
														<option value="Signed Authority Letter">Signed Authority Letter</option>
														<option value="Bank Letter">Bank Letter</option>
													</select> 
                                                    
											<label class="input">
												<input type="file" name="upload_1" id="upload_1" class="ipfile" />  
											</label> 
                                           
                                          <div id="showdeleteupload_1" style="display:none"><a id="delete_upload_1" href="javascript: void(0);">Delete</a></div>
										</section>
                                        </div>
                                     <div style="float:left">  | + <a href="javascript: void(0);" id="add_more_2" style="text-decoration:underline" ><em>Add More</em></a></div> </div> <br />
                                     <div id="fileinput2" style="display:none">
										<div class="row">
										<section class="col col-6"> Select doc type
                                        <select name="doc_type2" id="doc_type2" class="hello">
														<option value="" >Select</option>
														<option value="ID Proof">ID Proof</option>
														<option value="Address Proof">Address Proof</option>
														<option value="Stamp Pager doc">Stamp Pager doc</option>
														<option value="Signed Authority Letter">Signed Authority Letter</option>
														<option value="Bank Letter">Bank Letter</option>
													</select> 
                                                    
											<label class="input">
												<input type="file" name="upload_2" id="upload_2" class="ipfile" />  
											</label> 
                                          <div id="showdeleteupload_2" style="display:none"><a id="delete_upload_2" href="javascript: void(0);">Delete</a></div>
                                         </section>
										
                                        </div>
                                     <div style="float:left">  | + <a href="javascript: void(0);" id="add_more_3" style="text-decoration:underline" ><em>Add More</em></a></div> </div>
                                     <br />
                                     <div id="fileinput3" style="display:none">
										<div class="row">
										<section class="col col-6"> Select doc type
                                        <select name="doc_type3" id="doc_type3" class="hello">
														<option value="" >Select</option>
														<option value="ID Proof">ID Proof</option>
														<option value="Address Proof">Address Proof</option>
														<option value="Stamp Pager doc">Stamp Pager doc</option>
														<option value="Signed Authority Letter">Signed Authority Letter</option>
														<option value="Bank Letter">Bank Letter</option>
													</select> 
                                                    
											<label class="input">
												<input type="file" name="upload_3" id="upload_3" class="ipfile" />  
											</label> 
                                          <div id="showdeleteupload_3" style="display:none"><a id="delete_upload_3" href="javascript: void(0);">Delete</a></div>
                                         
										</section>
                                        </div>
                                     <div style="float:left">  | + <a href="javascript: void(0);" id="add_more_4" style="text-decoration:underline" ><em>Add More</em></a></div> </div><br />
                                     <div id="fileinput4" style="display:none">
										<div class="row">
										<section class="col col-6"> Select doc type
                                        <select name="doc_type4" id="doc_type4" class="hello">
														<option value="" >Select</option>
														<option value="ID Proof">ID Proof</option>
														<option value="Address Proof">Address Proof</option>
														<option value="Stamp Pager doc">Stamp Pager doc</option>
														<option value="Signed Authority Letter">Signed Authority Letter</option>
														<option value="Bank Letter">Bank Letter</option>
													</select> 
                                                    
											<label class="input">
												<input type="file" name="upload_4" id="upload_4" class="ipfile" />  
											</label> 
                                          <div id="showdeleteupload_4" style="display:none"><a id="delete_upload_4" href="javascript: void(0);">Delete</a></div>
                                         
										</section>
                                        </div>
                                     <div style="float:left">  | + <a href="javascript: void(0);" id="add_more_5" style="text-decoration:underline" ><em>Add More</em></a></div> </div>
                                     
                                        
									</fieldset>
					
									<footer>
                                        <button type="submit" class="btn btn-primary">
											Save
										</button>
    									<button type="button" class="btn btn-primary" id="edit_close_1">
											Close
										</button>
									</footer>
                                    </form>
                    </div>
                    

				</div>
<form id="edit_charity_hidden" action="docpost" name="edit_charity_hidden" class="smart-form" novalidate="novalidate" method="post" enctype="multipart/form-data" style="display:none">
  <input type="hidden" name="vendor_id" id="vendor_id" value="<?php  echo $vendor_id;?>" />
  <input type="text" name="org_name" id="org_name" placeholder="Enter Name" value="<?php echo $list[0]->Name; ?>">
  <input type="text" name="phone_number" id="phone_number" placeholder="Enter Number" value="<?php echo $list[0]->Phone_Number; ?>">
  <input type="text" name="trustee"  id="trustee" placeholder="Enter Name" value="<?php echo $list[0]->Trustee; ?>">
  <input type="text" name="reg_date" id="reg_date" value="<?php echo $list[0]->Registration_Date; ?>" >
  <textarea rows="3" cols="77" name="address"  id="address" placeholder="Enter Address"><?php echo $list[0]->Address; ?></textarea>
  <input type="text" name="org_type" id="org_type" placeholder="Enter Number only" value="<?php echo $list[0]->Type; ?>">
  <input type="text" name="percentage" id="percentage" placeholder="Enter Number only" value="<?php echo $list[0]->Contribution_Percent; ?>">
  <input type="text" name="bank_name" id="bank_name" placeholder="Enter Name" value="<?php echo $list[0]->Bank_Name; ?>">
  <input type="text" name="acc_holder_name" id="acc_holder_name" placeholder="Enter Name" value="<?php echo $list[0]->Account_Holder_Name; ?>">
  <input type="text" name="sort_code1" id="sort_code1" maxlength="2" placeholder="" value="<?php echo substr($list[0]->Sort_Code,0,2); ?>">
  -
  <input maxlength="2" type="text" name="sort_code2" id="sort_code2" placeholder="" value="<?php echo substr($list[0]->Sort_Code,2,2); ?>">
  -
  <input type="text" name="sort_code3" id="sort_code3" placeholder="" maxlength="2" value="<?php echo substr($list[0]->Sort_Code,4,2); ?>">
  <input type="text" name="acc_number" id="acc_number" placeholder="Enter Number" value="<?php echo $list[0]->Account_Number; ?>">
  <input type="text" name="doc_type" id="doc_type" placeholder="Enter Number" value="<?php echo $list[0]->Doc_Type; ?>">
  <input type="text" name="upload_doc" id="upload_doc" placeholder="Enter Number" value="<?php echo $list[0]->Files_Name; ?>">
  
</form>

<script>
  $(function() { //alert('hhisdfsdfiu');
		//$("#add_charity :input").attr("disabled", true);
		$('#show_edit_form').hide();
		$('#edit_button').click(function(){ 
		
		 var url = "<?php echo base_url(); ?>form/getform";  
		var getvendor_id = $("#vendor_id").val();
		//alert(getvendor_id);
		$.ajax({
				   type: "POST",
				   url: url,
				   data:"vendor_id="+getvendor_id,
				   success: function(data)
				   {
					 //alert(data);
					 $('#show_data_display').hide();
					 $('#show_edit_form').html(data);
					 $('#show_edit_form').show();
				   }
				 });
				return false 
			 $("#add_charity :input").attr("disabled", false);
			 $('#show_data_display').hide();
	 		 $('#show_edit_form').show();
			 //return false;
		});
		$('#edit_close').click(function(){ 
		$('#dialog').dialog('close');
		});

		$('#edit_close_1').click(function(){ 
		$('#dialog').dialog('close');
		});

$.validator.addMethod("chkRegex", function(value, element) {
        return this.optional(element) || /^[a-z0-9 \\-]+$/i.test(value);
    }, "Username must contain only letters, numbers, or dashes.");
	
$.validator.addMethod("chkRegexNumber", function(value, element) {
        return this.optional(element) || /^[0-9 \\-]+$/i.test(value);
    }, "Username must contain only letters, numbers, or dashes.");	
			
		var rules = {
				org_name: {
					 required: true,
					 chkRegex:true
				 },
				trustee : {
					required : true,
					chkRegex:true
				},
				phone_number : {
					required : true,
					chkRegexNumber:true
				},
				reg_date : {
					required : true
				},
				address : {
					required : true
				},
				org_type : {
					required : true
				},
				bank_name : {
					required : true
				},
				acc_holder_name : {
					required : true,
					chkRegex : true
				},
				sort_code1 : {
					required : true,
				},
				sort_code2 : {
					required : true,
				},
				sort_code3 : {
					required : true,
				},
				acc_number : {
					required : true,
					number : true
				},
				percentage : {
					required : true,
					number : true
				},
				doc_type0 : {
					required : true
				},
				upload_0 : {
					required : true
				},		 
     };
     var messages = {
	         org_name: {
    		         required: "Please enter name",
					 chkRegex : 'Please enter valid organisation name'
		        },
		 	trustee : {
					required : 'Please enter trustee name',
					chkRegex : 'Please enter valid trustee name'
				},
				phone_number : {
					required : 'Please enter phone number',
					chkRegexNumber : 'Please enter a VALID number'
				},
				reg_date : {
					required : 'Please select registration date'
				},
				address : {
					required : 'Please enter your full address'
				},
				org_type : {
					required : 'Please select type'
				},
				bank_name : {
					required : 'Please select bank'
				},
				acc_holder_name : {
					required : 'Please enter name',
					chkRegex : 'Please enter valid trustee name'
				},
				sort_code1 : {
					required : 'Please enter sort code',
					number : 'Please enter valid number'
				},
				sort_code2 : {
					required : 'Please enter sort code',
					number : 'Please enter valid number'
				},
				sort_code3 : {
					required : 'Please enter sort code',
					number : 'Please enter valid number'
				},
				acc_number : {
					required : 'Please enter account number',
					number : 'Please enter valid number'
				},
				percentage : {
					required : 'Please enter percentage',
					number : 'Please enter valid number'
				},
				doc_type0 : {
					required : 'Please select doc type'
				},
				upload_0 : {
					required : 'Please select doc'
				},
     };
     $("#edit_charity").validate({
         rules: rules,
         messages: messages
     });

     $("#btn").click(function () {

         $("#dlg").dialog({
             modal: true,
             buttons: {
                 "Save": function () {
                     alert($("#frm").valid());
                 },
                     "Cancel": function () {
                     $("#dlg").dialog("close");
                 }
             }

         });
     });

$("#edit_charity").submit(function(e) { 
	e.preventDefault();
 var url = "<?php echo base_url(); ?>add_charity/update";  

	 var form = $('#edit_charity');
        var isValid = form.valid();
        if (isValid) { alert('hi');
			$.ajax({
				   type: "POST",
				   url: url,
	               data: new FormData(this),
				   contentType: false,
        		   cache: false,
		           processData:false,
				   success: function(data)
				   {
					 alert(data);
				   }
				 });
		
			return false; // avoid to execute the actual submit of the form.
		}
});

$('a[id^="delete_upload_"]').click(function(){ //alert('hi');

var gethrefVal = $(this).attr('href');
	//alert(gethrefVal);
	//alert(this.id);
	
	var getId = this.id;
	getId = getId.split('_');
	getId = getId[2];
	//alert(getId);
	//$('#showdoctype_'+getId).hide();
	var getupload_doc = $('#upload_doc').val();
	var getsepearateval = gethrefVal.split(',');
	//alert(getsepearateval[0]);
	var finalupload_doc = getupload_doc.replace(getsepearateval[0],'');
	
	
	var getDoctype = $('#doc_type').val();
	//alert(getsepearateval[0]);
	var finaldoctype = getDoctype.replace(getsepearateval[1],'');
	
	//alert(finaldoctype);
	$('#doc_type').val(finaldoctype);
	$('#upload_doc').val(finalupload_doc);
	
	//alert($('#upload_doc').val());
	
 var url = "<?php echo base_url(); ?>deletedocpost";  
	 	$.ajax({
				   type: "POST",
				   url: url,
	               data: $("#edit_charity_hidden").serialize(),
				   success: function(data)
				   {
					   if(data == 0)
					   {
					   alert("Document deleted successfully");
					   $('#showdoctype_'+getId).hide();
					   
					     if($('#upload_doc').val() == ',' || $('#upload_doc').val() == '')
						{
							$('#showuplodeddoc_none').show();
							$('#showuplodeddoc_none').text('You dont have any uploaded doc!.');
						}
					   
					   }
					   else
					   {
						   alert('Error in deleting');
						   }
				   }
				 });
return false;
   
});	

$('a[id^="add_more_"]').click(function(){ //alert('hi');
	var getId = this.id;
	getId = getId.split('_');
	getId = getId[2];
	//alert(getId);
	if(getId<5)
	$("#fileinput"+getId).show();
	else
	 alert('Only upto five documents are allowed to upload!')	
    
});

 });

</script>

<style>
.invalid
{
	color:red
	
}
</style>