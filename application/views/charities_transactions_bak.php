<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>/css/data_table_page.css">
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>/css/data_table.css">
<!--<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>js/jquery.js"></script>
--><script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>js/jquery.dataTables.js"></script>
<script type="text/javascript">

			$(document).ready(function() {
				var oTable = $('#example').dataTable( {
					"sDom": 'R<C>H<"clear"><"ui-toolbar ui-widget-header ui-corner-tl ui-corner-tr ui-helper-clearfix"lfr>t<"ui-toolbar ui-widget-header ui-corner-bl ui-corner-br ui-helper-clearfix"ip>',
        "bAutoWidth": false,
        "bJQueryUI": true,
        "bLengthChange": false,

					 "sPaginationType": "full_numbers"
					
					});
			});
		</script>
<!--<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
--><script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<div id="main" role="main">
	<?php
		//configure ribbon (breadcrumbs) array("name"=>"url"), leave url empty if no url
		//$breadcrumbs["New Crumb"] => "http://url.com"
		$breadcrumbs["Misc"] = "";
		//include("inc/ribbon.php");
	?>

	<!-- MAIN CONTENT -->
	<div id="content">

		<!-- row -->
		
		<div class="row">
		
			<div class="col-sm-12"><br>
		
				<ul id="myTab1" class="nav nav-tabs bordered">
					<li>
						<a href="charities_trusts" >Trust </a>
					</li>
					<li class="active">
						<a href="charities_transactions" data-toggle="tab">Transactions</a>
					</li>
				</ul>
		
				<div id="myTabContent1" class="tab-content bg-color-white padding-10">
					<div class="tab-pane fade in active" id="s1">
						<h1> <b><i>Transactions</i></b></h1>
                        <form action="charities_transactions" name="search_transaction" id="search_transaction" method="post" onsubmit="return validateForm()" >
					     
                       <Table width="100%" style="border-top:0px;" class="table table-bordered">
                       <tr>
                       <td width="15%">Start Date &nbsp;</td>
                       <td width="30%"><input type="text" name="start_date" id="start_date"  value="<?php if($this->input->post('start_date') != '') echo $this->input->post('start_date'); ?>" /></td>                       
                       <td width="15%">End Date &nbsp;</td>
                       <td width="30%"><input type="text" name="end_date" id="end_date" value="<?php if($this->input->post('end_date') != '') echo $this->input->post('end_date'); ?>"  /></td>                       
                       <td width="10%"><input type="submit" value="Go" class="btn btn-primary btn-sm" name="submit"  /></td>                       
                       </tr>
                       </Table>
                          
                        </form>
                        <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
	<thead>
		<tr>
			<th>Transaction ID</th>
			<th>Transaction Date</th>
			<th>Trust Name</th>
			<th>Vendor ID</th>
			<th>Customer Mobile Number</th>
			<th>Customer ICICID Number</th>
			<th>Top Up Amount(%)</th>                        
			<th>Contributed Amount(%)</th>                        
		</tr>
	</thead>
	<tbody>
     <?php //echo '<pre>';print_r($list); echo count($list); exit;
	 		
			if(isset($list->ErrCode))
			{  $total_amount = 0;
				?>
			<?php }
			else
			{
		   $total_amount = 0;
		   for($i=0;$i<count($list);$i++)
		   {
			   if($list[$i]->Vendor_Id == $this->session->userdata('charities_vendor_id'))
			   {
				   
		 ?>
		<tr>
			<td><?php echo $list[$i]->Transaction_Id; ?></td>
			<td><?php echo str_replace('T',' ',$list[$i]->Transaction_Date); ?></td>
			<td><?php echo $list[$i]->Trust_Name; ?></td>
			<td><?php echo $list[$i]->Vendor_Id; ?></td>
			<td><?php echo $list[$i]->Customer_Mobile_Number; ?></td>
            <td><?php echo $list[$i]->Customer_ICCID_Number; ?></td>
			<td><?php echo $list[$i]->Top_Up_Amount; ?></td>
			<td><?php $total_amount = $total_amount + round($list[$i]->Contributed_Amount,2); echo round($list[$i]->Contributed_Amount,2); ?></td>

		</tr>
      <?php  }
		   }
			}
		    ?>
		
	</tbody>
</table>
                    </div>
<div align="right">Total Amount: <?php echo $total_amount;  ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
					
				</div>
		
			</div>
		
		</div>
		
		<!-- end row -->
	</div>
	<!-- END MAIN CONTENT -->
    

</div>
<!-- PAGE FOOTER -->
<script type="text/javascript">
function validateForm(){ 
 var x = document.forms["search_transaction"]["start_date"].value;
    if (x == null || x == "") {
       alert('Please select start date!');
	    document.getElementById('start_date').focus();
        return false;
    }
	var xy = document.forms["search_transaction"]["end_date"].value;
    if (xy == null || xy == "") {
       alert('Please select end date!');
	   document.getElementById('end_date').focus();
        return false;
    }
 }	
	$(document).ready(function() {
		$('#start_date,#end_date').datepicker({
			changeYear:true,
			maxDate: '+0d',
			yearRange: "2012:2020",
			dateFormat : 'dd/mm/yy',
			prevText : '<i class="fa fa-chevron-left"></i>',
			nextText : '<i class="fa fa-chevron-right"></i>',
		});
	});

	
</script>
<style>
.invalid
{
	color:red;
	
	}
</style>