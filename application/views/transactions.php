<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>/css/data_table_page.css">
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>/css/data_table.css">
<!--<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>js/jquery.js"></script>
--><script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>js/jquery.dataTables.js"></script>
<script type="text/javascript">

			$(document).ready(function() {
				var oTable = $('#example').dataTable( {
					"sDom": 'R<C>H<"clear"><"ui-toolbar ui-widget-header ui-corner-tl ui-corner-tr ui-helper-clearfix"lfr>t<"ui-toolbar ui-widget-header ui-corner-bl ui-corner-br ui-helper-clearfix"ip>',
        "bAutoWidth": false,
        "bJQueryUI": true,
        "bLengthChange": false,

					 "sPaginationType": "full_numbers"
					
					});
			});
		</script>
<!--<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
--><script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<div id="main" role="main">
	<?php
		//configure ribbon (breadcrumbs) array("name"=>"url"), leave url empty if no url
		//$breadcrumbs["New Crumb"] => "http://url.com"
		$breadcrumbs["Misc"] = "";
		//include("inc/ribbon.php");
	?>

	<!-- MAIN CONTENT -->
	<div id="content">

		<!-- row -->
		
		<div class="row">
		
			<div class="col-sm-12"><br>
		
				<ul id="myTab1" class="nav nav-tabs bordered">
					<li>
						<a href="trusts" >Charity Trusts </a>
					</li>
					<li class="active">
						<a href="transactions" data-toggle="tab">Transactions</a>
					</li>
				</ul>
		
				<div id="myTabContent1" class="tab-content bg-color-white padding-10">
					<div class="tab-pane fade in active" id="s1">
						<h1> <b><i>Transactions</i></b></h1>
                        <form action="transactions" name="search_transaction" id="search_transaction" method="post" onsubmit="return validateForm()" >
					     
                       <Table width="100%" style="border-top:0px;" class="table table-bordered">
                       <tr>
                       <td width="50%" align="left">
                       <!-- Table -1 -->
                       <Table cellpadding="0"  cellspacing="5">
                       <tr>
                       <td>Search Transactions &nbsp;</td>
                       <td><select name="search_type" class="form-control" id="search_type">
                                          <option value="">Select</option>
                                          <option <?php if($this->input->post('search_type') == 'Transaction ID') { ?> selected="selected" <?php } ?> value="Transaction ID">Transaction ID</option>
                                          <option value="Trust Name" <?php if($this->input->post('search_type') == 'Trust Name') { ?> selected="selected" <?php } ?>>Trust Name</option>
                                          <option value="Vendor ID" <?php if($this->input->post('search_type') == 'Vendor ID') { ?> selected="selected" <?php } ?>>Vendor ID </option>                          <option value="Mobile Number" <?php if($this->input->post('search_type') == 'Mobile Number') { ?> selected="selected" <?php } ?>>Mobile Number</option>
                                          <option value="ICCID Number" <?php if($this->input->post('search_type') == 'ICCID Number') { ?> selected="selected" <?php } ?>>ICCID Number </option>                                                    
                                          </select></td>                       
                       </tr>
                       </Table>
                       
                       
                       </td>
                        <td width="50%" align="right">
                       <!-- TAble -2 -->
                       <Table>
                       <tr>
                       <td><input type="text" name="search_value" class="form-control" placeholder="Enter value"  id="search_value" value="<?php if($this->input->post('search_value') != '') echo $this->input->post('search_value'); ?>" /></td>
                       <td><input type="submit" value="Go" class="btn btn-primary btn-sm" name="submit"  /></td>                       
                       </tr>
                       </Table>
                       
                       </td>                       
                       </tr>
                       </Table>
                          
                        </form>
                        <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
	<thead>
		<tr>
			<th>Transaction ID</th>
			<th>Transaction Date</th>
			<th>Trust Name</th>
			<th>Vendor ID</th>
			<th>Customer Mobile Number</th>
			<th>Customer ICICID Number</th>
			<th>Top Up Amount(%)</th>                        
			<th>Contributed Amount(%)</th>                        
		</tr>
	</thead>
	<tbody>
     <?php //echo '<pre>';print_r($list); echo count($list); exit;
	 		
			if(isset($list->ErrCode))
			{
				?>
                <tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
   			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td align="right">No records found</td>
   			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
			<?php }
			else
			{
		   for($i=0;$i<count($list);$i++)
		   {
			   
		 ?>
		<tr>
			<td><?php echo $list[$i]->Transaction_Id; ?></td>
			<td><?php  echo str_replace('T',' ',$list[$i]->Transaction_Date); ?></td>
			<td><?php echo $list[$i]->Trust_Name; ?></td>
			<td><?php echo $list[$i]->Vendor_Id; ?></td>
			<td><?php echo $list[$i]->Customer_Mobile_Number; ?></td>
            <td><?php echo $list[$i]->Customer_ICCID_Number; ?></td>
			<td><?php echo $list[$i]->Top_Up_Amount; ?></td>
			<td><?php echo round($list[$i]->Contributed_Amount,2); ?></td>

		</tr>
      <?php  }
			}
		    ?>
		
	</tbody>
</table>
                    </div>
		
					<div class="tab-pane fade" id="s2">
						<h1> <b><i>Transactions</i></b></h1>
						<br>
                     </div>
                     <?php if(!isset($list->ErrCode)){  ?>
						<button type="button" class="btn btn-primary" name="charity_donwnload" id="charity_donwnload" value="Download" onclick="window.location='export_excel/get_transaction_excel'" >
                        	<i class="fa fa-download"></i> Download</button> <?php } ?>

					
				</div>
		
			</div>
		
		</div>
		
		<!-- end row -->
	</div>
	<!-- END MAIN CONTENT -->
    

</div>
<!-- PAGE FOOTER -->
<script type="text/javascript">
function validateForm(){ 
 var x = document.forms["search_transaction"]["search_type"].value;
    if (x == null || x == "") {
       alert('Please select transaction!');
        return false;
    }
	var xy = document.forms["search_transaction"]["search_value"].value;
    if (xy == null || xy == "") {
       alert('Please enter value!');
	   document.getElementById('search_value').focus();
        return false;
    }
 }	
$.validator.addMethod("chkRegex", function(value, element) {
        return this.optional(element) || /^[a-z0-9 \\-]+$/i.test(value);
    }, "Username must contain only letters, numbers, or dashes.");
	
	$(document).ready(function() {

		var $checkoutForm = $('#search_transactions').validate({
		// Rules for form validation
			rules : {
				search_type : {
					required : true
				},
				search_value : {
					required : true,
					chkRegex:true
				},
			},

			// Messages for form validation
			messages : {
				search_type : {
					required : 'Please select type',
				},
				search_value : {
					required : 'Please enter search value',
					chkRegex : 'Please enter valid search value'
				},
			},

			// Do not change code below
			errorPlacement : function(error, element) {
				error.insertAfter(element.parent());
			}
			 
		});
		
	});

	
</script>
<style>
.invalid
{
	color:red;
	
	}
</style>