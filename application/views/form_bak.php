<div id="show_data_display" class="tab-content bg-color-white padding-10">
					<div class="tab-pane fade in active" id="s1">
						<br><div id="buttonPlaceholder" align="right"></div>
                        <?php  //print_r($list);?>
                        <form id="show_charity" action="docpost" name="show_charity" class="smart-form" novalidate="novalidate" method="post" enctype="multipart/form-data">
									<fieldset> <strong>Generic Info </strong>
										<div class="row">
											<section class="col col-5">Organisation Name
												<label class="input">
													<?php echo $list[0]->Name; ?>
												</label>
											</section>
											<section class="col col-5">Phone Number
												<label class="input"> 
													<?php echo $list[0]->Phone_Number; ?>
                                                </label>
											</section>
										</div>

										<div class="row">
											<section class="col col-6">
												<label class="input"> Trustee
													<?php echo $list[0]->Trustee; ?>
												</label>
											</section>
											<section class="col col-6">Registration Date
                                            <?php echo $list[0]->Registration_Date; ?>
											</section>
										</div>
                                        <div class="row">
											<section class="col col-6">
												<label class="input"> Address
													<?php echo $list[0]->Address; ?>
												</label>
											</section>
											<section class="col col-6">Type
												
											<div class="inline-group">
												<label class="input	">
													<?php if($list[0]->Type==1) echo "Non Profit Organisation"; elseif($list[0]->Type==2) echo "Charity"; elseif($list[0]->Type==3) echo "Other";?></label>
											</div>
										</section>
										</div>
                                         <div class="row">
										<section class="col col-6"> Contribution Percentage
											<?php echo $list[0]->Contribution_Percent; ?>
										</section>
                                        </div>
                                        
									</fieldset>

									<fieldset> <strong>Finance  Info</strong>
										<div class="row">
											<section class="col col-5">Bank Name
												<?php echo $list[0]->Bank_Name; ?>
											</section>
                                            

										</div>
										<div class="row">
										<section class="col col-6"> Account Holder Name
											<label class="input">
												<?php echo $list[0]->Account_Holder_Name; ?>
											</label>
                                            
										</section>
                                        </div>
										<div class="row">
										<section class="col col-6"> Sort Code
											<label class="textarea"> 										
												<?php echo $list[0]->Sort_Code; ?>
											</label>
										</section>
                                        </div>
                                        <div class="row">
										<section class="col col-6"> Account Number
											<label class="input">
												<?php echo $list[0]->Account_Number; ?>
											</label>
                                            
										</section>
                                        </div>
									</fieldset>
									<fieldset> <strong>Uploaded Documents</strong>
                                    <div id="fileinput0">
										<div class="row">
										<section class="col col-12"> 
                                       
                                       <?php 
									   echo $getfilename = $list[0]->Files_Name;
									   $findme   = ',';
									   $pos = strpos($getfilename, $findme);
									   
									   if ($pos === false) {
										   $showFilename = $getfilename;
										   if (file_exists('uploaded_file/'.$showFilename)) {
										   echo '<p id="showdoctype_0">Doc type &nbsp;';
											
											echo '&nbsp;&nbsp;<a href="uploaded_file/'.$showFilename.'" download>Download</a> &nbsp; &nbsp;';
											 echo $showFilename; echo '&nbsp;<a id="delete_upload_0" href="'.$showFilename.'">Delete</a></p>';
											 }
										} else {
											$getSeperateFiles = explode(',',$getfilename);
											//echo count($getSeperateFiles);
											for($j=0;$j<count($getSeperateFiles);$j++)
											{
												//echo $getSeperateFiles[$j];
												 if (file_exists('uploaded_file/'.$getSeperateFiles[$j])) {
												echo '<p id="showdoctype_'.$j.'">Doc type &nbsp;&nbsp;';
											    echo $getSeperateFiles[$j];
												 echo '&nbsp;&nbsp;<a href="uploaded_file/'.$getSeperateFiles[$j].'" download>Download</a> &nbsp; &nbsp;';
											
												echo '&nbsp; &nbsp; <a id="delete_upload_'.$j.'" href="'.$getSeperateFiles[$j].'">Delete</a></p>';
												echo '<br>';
												 }
												}
											
										}
									   ?>
										</section>
                                        </div>
                                      </div>
                                      <br />
									</fieldset>
                                    <fieldset> <strong>Account Creation Details</strong>
                                    <div id="fileinput0">
										<div class="row">
										<section class="col col-6"> <p>Created By
                                       <?php /*echo $list[0]->Created_By;*/ echo $this->session->userdata('username'); ?></p>
                                     <p>Modified By  <?php echo $list[0]->Modified_By; ?></p>
                                     <p>Created On  <?php echo $list[0]->Created_On; ?></p>
                                     <p>Last Modified Date  <?php echo $list[0]->Modified_Date; ?></p>                                                                          
                                     	</section>
                                        </div>
                                      </div>
                                      <br />
									</fieldset>
								</form>
									<footer>
										<button type="submit" class="btn btn-primary" id="edit_button">
											Edit
										</button>
    									<button type="button" class="btn btn-primary" id="edit_close">
											Close
										</button>
										
									</footer>
								
                                
                                
                        
                        
                    </div>
		
					
				</div>
<div id="show_edit_form" class="tab-content bg-color-white padding-10">
					<div class="tab-pane fade in active" id="s1">
						<br><div id="buttonPlaceholder" align="right"></div>
                        <form id="edit_charity" action="docpost" name="edit_charity" class="smart-form" novalidate="novalidate" method="post" enctype="multipart/form-data">
<input type="hidden" name="vendor_id" id="vendor_id" value="<?php  echo $vendor_id;?>" />
									<fieldset> <strong>Generic Info </strong>
										<div class="row">
											<section class="col col-5">Organisation Name
												<label class="input">
													<input type="text" name="org_name" id="org_name" placeholder="Enter Name" value="<?php echo $list[0]->Name; ?>">
												</label>
											</section>
											<section class="col col-5">Phone Number
												<label class="input"> 
													<input type="text" name="phone_number" id="phone_number" placeholder="Enter Number" value="<?php echo $list[0]->Phone_Number; ?>">
												</label>
											</section>
										</div>

										<div class="row">
											<section class="col col-6">
												<label class="input"> Trustee
													<input type="text" name="trustee"  id="trustee" placeholder="Enter Name" value="<?php echo $list[0]->Trustee; ?>">
												</label>
											</section>
											<section class="col col-6">Registration Date
                                            
                                            <label class="input"> <i class="icon-append fa fa-calendar"></i>
													<input type="text" name="reg_date" id="reg_date" value="<?php echo $list[0]->Registration_Date; ?>" >
												</label>
											</section>
										</div>
                                        <div class="row">
											<section class="col col-6">
												<label class="input"> Address
													<textarea rows="3" cols="77" name="address"  id="address" placeholder="Enter Address"><?php echo $list[0]->Address; ?></textarea> 
												</label>
											</section>
											<section class="col col-6">Type
												
											<div class="inline-group">
												<label class="radio">
													<input type="radio" name="org_type" <?php if($list[0]->Type==1) { ?> checked="checked"<?php } ?>value="1">
													<i></i>Non Profit Organisation</label>
												<label class="radio">
													<input type="radio" name="org_type" <?php if($list[0]->Type==2) { ?> checked="checked"<?php } ?> value="2">
													<i></i>Charity</label>
												<label class="radio">
													<input type="radio" name="org_type" <?php if($list[0]->Type==3) { ?> checked="checked"<?php } ?> value="3">
													<i></i>Other</label>
											</div>
										</section>
										</div>
                                         <div class="row">
										<section class="col col-6"> Percentage
											<label class="input">
												<input type="text" name="percentage" id="percentage" placeholder="Enter Number only" value="<?php echo $list[0]->Contribution_Percent; ?>"> 
											</label>
                                            
										</section>
                                        </div>
									</fieldset>

									<fieldset> <strong>Finance  Info</strong>
										<div class="row">
											<section class="col col-5">Bank Name
                                            <input type="text" name="bank_name" id="bank_name" placeholder="Enter Name" value="<?php echo $list[0]->Bank_Name; ?>"> 
												
											</section>
                                            

										</div>
										<div class="row">
										<section class="col col-6"> Account Holder Name
											<label class="input">
												<input type="text" name="acc_holder_name" id="acc_holder_name" placeholder="Enter Name" value="<?php echo $list[0]->Account_Holder_Name; ?>">
											</label>
                                            
										</section>
                                        
                                        </div>
										<div class="row">

										<section class="col col-6"> Sort Code
											<label class="textarea"> 										
												<input type="text" name="sort_code1" id="sort_code1" maxlength="2" placeholder="" value="<?php echo substr($list[0]->Sort_Code,0,2); ?>"> - <input maxlength="2" type="text" name="sort_code2" id="sort_code2" placeholder="" value="<?php echo substr($list[0]->Sort_Code,2,2); ?>"> - <input type="text" name="sort_code3" id="sort_code3" placeholder="" maxlength="2" value="<?php echo substr($list[0]->Sort_Code,4,2); ?>">
											</label>
										</section>
                                        </div>
                                        <div class="row">
										<section class="col col-6"> Account Number
											<label class="input">
												<input type="text" name="acc_number" id="acc_number" placeholder="Enter Number" value="<?php echo $list[0]->Account_Number; ?>">
											</label>
                                            
										</section>
                                        </div>
                                       
									</fieldset>
									<fieldset> <strong>Upload Documents</strong>
                                    <div id="fileinput0">
										<div class="row">
										<section class="col col-6"> Select doc type
                                        <select name="doc_type0" id="doc_type0" class="hello">
														<option value="" >Select</option>
														<option value="ID Proof">ID Proof</option>
														<option value="Address Proof">Address Proof</option>
														<option value="Stamp Pager doc">Stamp Pager doc</option>
														<option value="Signed Authority Letter">Signed Authority Letter</option>
														<option value="Bank Letter">Bank Letter</option>
													</select> 
                                                    
											<label class="input">
												<input type="file" name="upload_0" id="upload_0"  class="ipfile"/>  
											</label> 
                                          
                                          <div id="showdeleteupload_0" style="display:none"><a id="delete_upload_0" href="javascript: void(0);">Delete</a></div>
                                         
										</section>
                                        </div>
                                     <div style="float:left">  | + <a href="javascript: void(0);" id="add_more_1" style="text-decoration:underline" ><em>Add More</em></a></div> </div>
                                      <br />
                                      <div id="fileinput1" style="display:none">
										<div class="row">
										<section class="col col-6"> Select doc type
                                        <select name="doc_type1" id="doc_type1" class="hello">
														<option value="" >Select</option>
														<option value="ID Proof">ID Proof</option>
														<option value="Address Proof">Address Proof</option>
														<option value="Stamp Pager doc">Stamp Pager doc</option>
														<option value="Signed Authority Letter">Signed Authority Letter</option>
														<option value="Bank Letter">Bank Letter</option>
													</select> 
                                                    
											<label class="input">
												<input type="file" name="upload_1" id="upload_1" class="ipfile" />  
											</label> 
                                           
                                          <div id="showdeleteupload_1" style="display:none"><a id="delete_upload_1" href="javascript: void(0);">Delete</a></div>
										</section>
                                        </div>
                                     <div style="float:left">  | + <a href="javascript: void(0);" id="add_more_2" style="text-decoration:underline" ><em>Add More</em></a></div> </div> <br />
                                     <div id="fileinput2" style="display:none">
										<div class="row">
										<section class="col col-6"> Select doc type
                                        <select name="doc_type2" id="doc_type2" class="hello">
														<option value="" >Select</option>
														<option value="ID Proof">ID Proof</option>
														<option value="Address Proof">Address Proof</option>
														<option value="Stamp Pager doc">Stamp Pager doc</option>
														<option value="Signed Authority Letter">Signed Authority Letter</option>
														<option value="Bank Letter">Bank Letter</option>
													</select> 
                                                    
											<label class="input">
												<input type="file" name="upload_2" id="upload_2" class="ipfile" />  
											</label> 
                                          <div id="showdeleteupload_2" style="display:none"><a id="delete_upload_2" href="javascript: void(0);">Delete</a></div>
                                         </section>
										
                                        </div>
                                     <div style="float:left">  | + <a href="javascript: void(0);" id="add_more_3" style="text-decoration:underline" ><em>Add More</em></a></div> </div>
                                     <br />
                                     <div id="fileinput3" style="display:none">
										<div class="row">
										<section class="col col-6"> Select doc type
                                        <select name="doc_type3" id="doc_type3" class="hello">
														<option value="" >Select</option>
														<option value="ID Proof">ID Proof</option>
														<option value="Address Proof">Address Proof</option>
														<option value="Stamp Pager doc">Stamp Pager doc</option>
														<option value="Signed Authority Letter">Signed Authority Letter</option>
														<option value="Bank Letter">Bank Letter</option>
													</select> 
                                                    
											<label class="input">
												<input type="file" name="upload_3" id="upload_3" class="ipfile" />  
											</label> 
                                          <div id="showdeleteupload_3" style="display:none"><a id="delete_upload_3" href="javascript: void(0);">Delete</a></div>
                                         
										</section>
                                        </div>
                                     <div style="float:left">  | + <a href="javascript: void(0);" id="add_more_4" style="text-decoration:underline" ><em>Add More</em></a></div> </div><br />
                                     <div id="fileinput4" style="display:none">
										<div class="row">
										<section class="col col-6"> Select doc type
                                        <select name="doc_type4" id="doc_type4" class="hello">
														<option value="" >Select</option>
														<option value="ID Proof">ID Proof</option>
														<option value="Address Proof">Address Proof</option>
														<option value="Stamp Pager doc">Stamp Pager doc</option>
														<option value="Signed Authority Letter">Signed Authority Letter</option>
														<option value="Bank Letter">Bank Letter</option>
													</select> 
                                                    
											<label class="input">
												<input type="file" name="upload_4" id="upload_4" class="ipfile" />  
											</label> 
                                          <div id="showdeleteupload_4" style="display:none"><a id="delete_upload_4" href="javascript: void(0);">Delete</a></div>
                                         
										</section>
                                        </div>
                                     <div style="float:left">  | + <a href="javascript: void(0);" id="add_more_5" style="text-decoration:underline" ><em>Add More</em></a></div> </div>
                                     
                                        
									</fieldset>
					
									<footer>
                                        <button type="submit" class="btn btn-primary">
											Save
										</button>
    									<button type="button" class="btn btn-primary" id="edit_close_1">
											Close
										</button>
									</footer>
                                    </form>
                    </div>
                    

				</div>


<script>
  $(function() { //alert('hhisdfsdfiu');
		//$("#add_charity :input").attr("disabled", true);
		$('#show_edit_form').hide();
		$('#edit_button').click(function(){ 
			 $("#add_charity :input").attr("disabled", false);
			 $('#show_data_display').hide();
	 		 $('#show_edit_form').show();
			 //return false;
		});
		$('#edit_close').click(function(){ 
		$('#dialog').dialog('close');
		});

		$('#edit_close_1').click(function(){ 
		$('#dialog').dialog('close');
		});

$.validator.addMethod("chkRegex", function(value, element) {
        return this.optional(element) || /^[a-z0-9 \\-]+$/i.test(value);
    }, "Username must contain only letters, numbers, or dashes.");
	
$.validator.addMethod("chkRegexNumber", function(value, element) {
        return this.optional(element) || /^[0-9 \\-]+$/i.test(value);
    }, "Username must contain only letters, numbers, or dashes.");	
			
		var rules = {
				org_name: {
					 required: true,
					 chkRegex:true
				 },
				trustee : {
					required : true,
					chkRegex:true
				},
				phone_number : {
					required : true,
					chkRegexNumber:true
				},
				reg_date : {
					required : true
				},
				address : {
					required : true
				},
				org_type : {
					required : true
				},
				bank_name : {
					required : true
				},
				acc_holder_name : {
					required : true,
					chkRegex : true
				},
				sort_code1 : {
					required : true,
				},
				sort_code2 : {
					required : true,
				},
				sort_code3 : {
					required : true,
				},
				acc_number : {
					required : true,
					number : true
				},
				percentage : {
					required : true,
					number : true
				},
				doc_type0 : {
					required : true
				},
				upload_0 : {
					required : true
				},		 
     };
     var messages = {
	         org_name: {
    		         required: "Please enter name",
					 chkRegex : 'Please enter valid organisation name'
		        },
		 	trustee : {
					required : 'Please enter trustee name',
					chkRegex : 'Please enter valid trustee name'
				},
				phone_number : {
					required : 'Please enter phone number',
					chkRegexNumber : 'Please enter a VALID number'
				},
				reg_date : {
					required : 'Please select registration date'
				},
				address : {
					required : 'Please enter your full address'
				},
				org_type : {
					required : 'Please select type'
				},
				bank_name : {
					required : 'Please select bank'
				},
				acc_holder_name : {
					required : 'Please enter name',
					chkRegex : 'Please enter valid trustee name'
				},
				sort_code1 : {
					required : 'Please enter sort code',
					number : 'Please enter valid number'
				},
				sort_code2 : {
					required : 'Please enter sort code',
					number : 'Please enter valid number'
				},
				sort_code3 : {
					required : 'Please enter sort code',
					number : 'Please enter valid number'
				},
				acc_number : {
					required : 'Please enter account number',
					number : 'Please enter valid number'
				},
				percentage : {
					required : 'Please enter percentage',
					number : 'Please enter valid number'
				},
				doc_type0 : {
					required : 'Please select doc type'
				},
				upload_0 : {
					required : 'Please select doc'
				},
     };
     $("#edit_charity").validate({
         rules: rules,
         messages: messages
     });

$('#reg_date').datepicker({
			dateFormat : 'mm/dd/yy',
			prevText : '<i class="fa fa-chevron-left"></i>',
			nextText : '<i class="fa fa-chevron-right"></i>',
		});
     $("#btn").click(function () {

         $("#dlg").dialog({
             modal: true,
             buttons: {
                 "Save": function () {
                     alert($("#frm").valid());
                 },
                     "Cancel": function () {
                     $("#dlg").dialog("close");
                 }
             }

         });
     });

$("#edit_charity").submit(function(e) { 
	e.preventDefault();
 var url = "<?php echo base_url(); ?>add_charity/update";  

	 var form = $('#edit_charity');
        var isValid = form.valid();
        if (isValid) { alert('hi');
			$.ajax({
				   type: "POST",
				   url: url,
	               data: new FormData(this),
				   contentType: false,
        		   cache: false,
		           processData:false,
				   success: function(data)
				   {
					 alert(data);
				   }
				 });
		
			return false; // avoid to execute the actual submit of the form.
		}
});

$('a[id^="delete_upload_"]').click(function(){ //alert('hi');

var gethrefVal = $(this).attr('href');
	alert(gethrefVal);
	alert(this.id);
	
	var getId = this.id;
	getId = getId.split('_');
	getId = getId[2];
	//alert(getId);
	//$('#showdoctype_'+getId).hide();
	
	
 var url = "<?php echo base_url(); ?>deletedocpost";  
	 	$.ajax({
				   type: "POST",
				   url: url,
	               data: "delete_doc="+gethrefVal,
				   success: function(data)
				   {
					   if(data == 0)
					   {
					   alert("Deleted successfully!");
   					   $('#showdoctype_'+getId).hide();
					   }
					   else
					   alert('Error in deleting');
				   }
				 });

return false;
   
});	

$('a[id^="add_more_"]').click(function(){ //alert('hi');
	var getId = this.id;
	getId = getId.split('_');
	getId = getId[2];
	//alert(getId);
	if(getId<5)
	$("#fileinput"+getId).show();
	else
	 alert('Only upto five documents are allowed to upload!')	
    
});

 });

</script>
<style>
.invalid
{
	color:red
	
}
</style>