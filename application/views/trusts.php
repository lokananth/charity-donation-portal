<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>/css/data_table_page.css">
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>/css/data_table.css">
<!--<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>js/jquery.js"></script>
--><script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>js/jquery.dataTables.js"></script>
<script type="text/javascript">

			$(document).ready(function() {
				var buttonPlaceholder = $("#buttonPlaceholder").html('<a href="add_charity"><button  class="btn btn-primary"><i class="fa fa-plus-square"></i> Add New</button></a>');
				var oTable = $('#example').dataTable( {
					"sDom": 'R<C><"#buttonPlaceholder">H<"clear"><"ui-toolbar ui-widget-header ui-corner-tl ui-corner-tr ui-helper-clearfix"lfr>t<"ui-toolbar ui-widget-header ui-corner-bl ui-corner-br ui-helper-clearfix"ip>',
        "bAutoWidth": false,
        "bJQueryUI": true,
        "bLengthChange": false,

					 "sPaginationType": "full_numbers"
					
					});
			});
		</script>
<!--<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">-->
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<div id="main" role="main">

	<!-- MAIN CONTENT -->
	<div id="content">

		<!-- row -->
		
		<div class="row">
		
			<div class="col-sm-12"><br />
		
				<ul id="myTab1" class="nav nav-tabs bordered">
					<li class="active">
						<a href="trusts" >Charity Trusts </a><!--<i class="fa fa-caret-down"></i> -->
					</li>
					<li>
						<a href="transactions">Transactions</a>
					</li>
				</ul>
		
				<div id="myTabContent1" class="tab-content bg-color-white padding-10">
					<div class="tab-pane fade in active" id="s1">
						<div class="col-sm-12">
	                        <div class="col-sm-6"><h1> <b><i>Charity Trusts</i></b></h1></div>
    	                    <div class="col-sm-6"><div id="buttonPlaceholder" align="right"></div></div>
                        </div>
                 <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Trustee</th>
                        <th>Address</th>
                        <th>Type</th>
                        <th>Phone Number</th>
                        <th>Vendor ID</th>
                        <th>Registration Date</th>                        
                        <th>Added By</th>                        
                        <th>Added On</th>                                                
                    </tr>
                </thead>
                <tbody>
    <?php //echo '<pre>';print_r($list); echo count($list); exit;
		
		if(!isset($list->ErrCode))
		{
		   
		   for($i=0;$i<count($list);$i++)
		   {
			   
		 ?>
		<tr>
			<td id="click_title_<?php echo $i; ?>" class="charity_name_click"><?php echo $list[$i]->Name; ?></td>
			<td><?php echo $list[$i]->Trustee; ?></td>
			<td><?php echo $list[$i]->Address; ?></td>
			<td><?php if($list[$i]->Type == 1) echo 'Non Profit Organisation'; elseif($list[$i]->Type == 2) echo 'Charity'; elseif($list[$i]->Type == 3) echo 'Other';  ?></td>
			<td><?php echo $list[$i]->Phone_Number; ?></td>
            <td id="vendor_id<?php echo $i;?>"><?php echo $list[$i]->Vendor_Id; ?></td>
			<td><?php echo $list[$i]->Registration_Date; ?></td>
			<td><?php echo $this->session->userdata('username'); ?></td>
			<td><?php  echo substr($list[$i]->Added_On,0,10); ?></td>
		</tr>
		<?php  }
		}
		else
		{
		   ?>
            <tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
   			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td align="right">No records found</td>
   			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
  			<td>&nbsp;</td>
		</tr>
		<?php 
		}
		?>
	</tbody>
</table>
                    </div>
		
					<div class="tab-pane fade" id="s2">
						<h1> <b><i>Transactions</i></b></h1>
						<br>
                     </div>
                     <?php if(!isset($list->ErrCode)) { ?>
                     <button type="button" class="btn btn-primary" onclick="window.location='export_excel'" name="charity_donwnload" id="charity_donwnload" value="Download" ><i class="fa fa-download"> </i> Download</button> <?php } ?>
				</div>
		
			</div>
		
		</div>
		
		<!-- end row -->
	</div>
	<!-- END MAIN CONTENT -->
    

</div>
<div id="dialog"></div>
<script src="<?php echo base_url(); ?>/js/plugin/jquery-form/jquery-form.min.js"></script>

<script type="text/javascript">
    $(function ()    {
		
		
     $("#btn").click(function () {

         $("#dlg").dialog({
             modal: true,
             buttons: {
                 "Save": function () {
                     alert($("#frm").valid());
                 },
                     "Cancel": function () {
                     $("#dlg").dialog("close");
                 }
             }

         });
     });
	 
	  var rules = {
         txtName: {
             required: true
         }
     };
     var messages = {
         txtName: {
             required: "Please enter name"
         }
     };
     $("#frm").validate({
         rules: rules,
         messages: messages
     });
	 
	 
		$('#example').delegate('tr td[id^="click_title_"]', 'click', function (e) {
		//$('tr td[id^="click_title_"]').click(function(){ 
		var getTitle = $(this).text();
		var getId = this.id;
		getId = getId.split('_');
		getId = getId[2];
		var getvendor = $('#vendor_id'+getId).text();
		
		data = 'vendor_id=' + getvendor;
        $('#dialog').dialog({
            modal: true,
            open: function ()
            { 
                //$(this).load('form.php');
				$.ajax({
		   type: "POST",
		   data:data,
		   url: "<?php echo base_url(); ?>form",
		   success: function(data){  
				$('#dialog').html(data);
		   }
		 });	
            },         
            height: 700,
            width: 900,
            title: getTitle,
        });
		});
    });
</script>