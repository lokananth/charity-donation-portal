<div id="show_edit_form" class="tab-content bg-color-white padding-10">
					<div class="tab-pane fade in active" id="s1">
						<br><div id="buttonPlaceholder" align="right"></div>
                        <form id="edit_charity_myform" action="docpost" name="edit_charity_myform" class="smart-form" novalidate method="post" enctype="multipart/form-data">
<input type="hidden" name="vendor_id" id="vendor_id" value="<?php  echo $vendor_id;?>" />
									 <legend>Generic Info</legend>  
                            <fieldset>
                            
                            <div class="col col-6">
										<div class="form-group">
                                        <section>Organisation Name
												<label class="input">
													<input readonly type="text" name="org_name" id="org_name" placeholder="Enter Name" value="<?php echo $list[0]->Name; ?>">
												</label>
											</section>
                                            
                                            <section>
												<label class="input"> Trustee
													<input type="text" name="trustee"  id="trustee" placeholder="Enter Name" value="<?php echo $list[0]->Trustee; ?>">
												</label>
											</section>
                                            <section>
												<label class="input"> Address</label>
												  <textarea rows="3" cols="67" name="address" style="resize:none;"  id="address" placeholder="Enter Address"><?php echo $list[0]->Address; ?></textarea> 
												
											</section>
                                            <section >
                                            Contribute Percentage
										<label class="input">
										<input type="text" name="percentage" id="percentage" placeholder="Enter Number only" value="<?php echo $list[0]->Contribution_Percent; ?>" maxlength="5"> 
										</label>
                                         </section> 
                                        
                                        </div>
                                        </div>
                                          <div class="col col-6">
										<div class="form-group">
                                        <section>Phone Number
												<label class="input"> 
												<input type="text" name="phone_number" id="phone_number" placeholder="Enter Number" value="<?php echo $list[0]->Phone_Number; ?>">
												</label>
											</section>
                                            <section>Registration Date
                                            <label class="input"> <i class="icon-append fa fa-calendar"></i>
													<input type="text" name="reg_date" id="reg_date" value="<?php  echo $list[0]->Registration_Date; ?>" >
												</label>
											</section>
                                            <section>Type
												<label class="radio">
													<input type="radio" name="org_type" <?php if($list[0]->Type==1) { ?> checked="checked"<?php } ?>value="1">
													<i></i>Non Profit Organisation</label> 
												<label class="radio">
													<input type="radio" name="org_type" <?php if($list[0]->Type==2) { ?> checked="checked"<?php } ?> value="2">
													<i></i>Charity</label>
												<label class="radio">
													<input type="radio" name="org_type" <?php if($list[0]->Type==3) { ?> checked="checked"<?php } ?> value="3">
													<i></i>Other</label>
										</section>
                                        
                                        </div>
                                        </div>
                           
								  
										 <!-- Set -1 Over -->	
					 </fieldset>
					 
						 <legend>Finance  Info</legend>
									<fieldset>
                                    <div class="">
										<section class="col col-8">Bank Name
										<label class="input">
										 <input type="text" name="bank_name" id="bank_name" placeholder="Enter Name" value="<?php echo $list[0]->Bank_Name; ?>">
										</label>
                                       </section>
                                    </div>
                                    <div class="">
										<section class="col col-8"> Account Holder Name
										<label class="input">
										 <input type="text" name="acc_holder_name" id="acc_holder_name" placeholder="Enter Name" value="<?php echo $list[0]->Account_Holder_Name; ?>">
										</label>
                                       </section>
                                    </div>
										 
                                         <div class="">
										<section class="col col-8">Sort Code
										<label class="input"> 
                                           <div class="col col-4 p-l-0">
                                           <input type="text" name="sort_code1" id="sort_code1" maxlength="2" placeholder="Sort Code1" value="<?php echo substr($list[0]->Sort_Code,0,2); ?>">
                                            </div>										
										   <div class="col col-4 p-l-0">
			                               <input maxlength="2" type="text" name="sort_code2" id="sort_code2" placeholder="Sort Code2" value="<?php echo substr($list[0]->Sort_Code,2,2); ?>">					
                                           </div>				 
                                           <div class="col col-4 p-l-0 p-r-0">
                                           <input type="text" name="sort_code3" id="sort_code3" placeholder="Sort Code3" maxlength="2" value="<?php echo substr($list[0]->Sort_Code,4,2); ?>">	
                                           </div>										
									   </label>
                                        </label>
                                       </section>
                                    </div>
										  <div class="">
										<section class="col col-8"> Account Number
										<label class="input">
										<input type="text" name="acc_number" id="acc_number" placeholder="Enter Number" maxlength="20" value="<?php echo $list[0]->Account_Number; ?>">
										</label>
                                       </section>
                                    </div>
										  
                                        
                                       
									</fieldset>
                                    
                                    <legend>Charity Login  Info</legend>
									<fieldset>
                                    <div class="">
										<section class="col col-8">Username
										<input type="hidden" name="charity_username" id="charity_username" value="<?php echo $list[0]->Username; ?>">

										<label class="input">
										 <?php echo $list[0]->Username; ?>
										</label>
                                       </section>
                                    </div>
                                    <div class="">
										<section class="col col-8"> Change Password
										<label class="input">
										<input type="hidden" name="charity_oldpassword" id="charity_oldpassword"  value="<?php echo $list[0]->Password; ?>">
                                        <input type="password" name="charity_newpassword" id="charity_newpassword" value="<?php echo $list[0]->Password; ?>">
										</label>
                                       </section>
                                    </div>
										 
										  <div class="">
										<section class="col col-8"> Email Id
										<label class="input">
										<input type="hidden" name="charity_email" id="charity_email" value="<?php echo $list[0]->Email; ?>">
                                        
										<?php echo $list[0]->Email; ?>
										</label>
                                       </section>
                                    </div>
										  
                                        
                                       
									</fieldset>
                                    <legend>Upload Documents</legend>
									<fieldset> 
             			  <input type="hidden" name="doc_type" id="doc_type" placeholder="Enter Number" value="<?php echo $list[0]->Doc_Type; ?>">
                          <input type="hidden" name="upload_doc" id="upload_doc" placeholder="Enter Number" value="<?php echo $list[0]->Files_Name; ?>">

                                      <br />
                                     
										<section class="col-lg-12">
                                     <?php
									   $getfilename = $list[0]->Files_Name;
									   $getdoctype = $list[0]->Doc_Type;
									   $findme   = ',';
									   $pos = strpos($getfilename, $findme);
									   
									   if ($pos === false) {
										   $showFilename = $getfilename;
										   if($showFilename!='') {
										   echo '<div class="row" id="showdoctype_0"><div class="col-sm-2" id="showdoctype_0">Doc type </div>';
										   echo '<div class="col-sm-3">'.$getdoctype.'</div>';
										   echo '<div class="col-sm-3 text-nowrap">'.$showFilename.'</div></div>';
										   $delete_array[] = $getdoctype;
										   $total_to_display = 4;
										  }
										   else
										   {
											   $total_to_display = 5;
											   $delete_array = array();
											   //echo '<div id="showuplodeddoc_none">You dont have any uploaded doc!.</div>';
											  }

										} else {
											$getSeperateFiles = explode(',',$getfilename);
											$getSeperateDoctype = explode(',',$getdoctype);
											//echo count($getSeperateFiles);
											 $total_to_display = 5;
											for($j=0;$j<count($getSeperateFiles);$j++)
											{
												//echo $getSeperateFiles[$j];
												echo '<div class="row" id="showdoctype_'.$j.'"><div class="col-sm-3">Doc type </div>';
												echo '<div class="col-sm-3">'.$getSeperateDoctype[$j].'</div>';
											    echo '<div class="col-sm-3">'.$getSeperateFiles[$j].'</div></div>';
												echo '<br>';
												$delete_array[] = $getSeperateDoctype[$j];
												$total_to_display = $total_to_display-1;
											 }
											 //echo '<br>'.$total_to_display;
										}
									   ?> 
								 	</section>
                                   
                                    <?php 
									
									$setarray = array('ID Proof','Address Proof','Stamp Pager doc','Signed Authority Letter','Bank Letter');
									//print_r($setarray);
									//print_r($delete_array);
									$getfinalarrresult = array_diff($setarray, $delete_array);

									$getDoctype = array_values($getfinalarrresult);
									//echo '<br>'.$total_to_display;
									//print_r($getDoctype);
									for($i=0;$i<$total_to_display;$i++) {
									?>
                                    <div id="fileinput<?php echo $i; ?>">
										<div class="row">
                                            <section class="col-sm-4"> 
											 
                                                <label class="pull-left"> Select doc type</label>
                                              
													<select name="doc_type<?php echo $i; ?>" class="form-control popup pull-right" id="doc_type<?php echo $i; ?>" class="hello">
													<option value="" >Select</option>
													<?php
													for($k=0;$k<count($getDoctype);$k++) { ?>
													<option value="<?php echo $getDoctype[$k]?>"><?php echo $getDoctype[$k]?></option>
													<?php } ?>
													</select> 
											
                                            </section>
                                            <section class="col-sm-4">  
                                                <div class="input input-file file-popup">
												<span class="button">
                                                        	<input type="file"  onchange="this.parentNode.nextSibling.value = this.value"   name="upload_<?php echo $i; ?>" id="upload_<?php echo $i; ?>">Browse</span><input type="text" id="upload_<?php echo $i; ?>_reset" readonly="" class="form-control"   placeholder="Include some files">
                                             <!--    <div class="col col-10"> <label class="input">
                                                        <input type="file" name="upload_<?php //echo $i; ?>" id="upload_<?php //echo $i; ?>"  class="ipfile"/>
                                                        
                                                    </label> </div> -->
                                                    
                                                </div> 
                                            
                                            </section>
                                           
										</div>
                                        </div>
                                       
                                     <?php   echo '<br>'; } ?>
                                     
                                     
                                        
									</fieldset>
					
									<footer>
                                        <button type="submit" class="btn btn-primary">
											Save
										</button>
    									<button type="button" class="btn btn-primary" id="edit_close_2">
											Close
										</button>
									</footer>
                                    </form>
                    </div>
<script>
$(function() { 

$.validator.addMethod("chkRegex", function(value, element) {
        return this.optional(element) || /^[a-z0-9 \\-]+$/i.test(value);
    }, "Username must contain only letters, numbers, or dashes.");
	
$.validator.addMethod("chkRegexName", function(value, element) {
        return this.optional(element) || /^[a-z \\-]+$/i.test(value);
    }, "Username must contain only letters, numbers, or dashes.");
		

$.validator.addMethod("chkRegexNumber", function(value, element) {
        return this.optional(element) || /^[0-9 \\-]+$/i.test(value);
    }, "Username must contain only letters, numbers, or dashes.");
		

		var $checkoutForm = $('#edit_charity_myform').validate({
		// Rules for form validation
			rules : {
				org_name : {
					required : true,
					chkRegexName:true
				},
				trustee : {
					required : true,
					chkRegexName:true
				},
				phone_number : {
					required : true,
					chkRegexNumber:true
				},
				reg_date : {
					required : true
				},
				address : {
					required : true
				},
				org_type : {
					required : true
				},
				percentage :{
					required : true
				},
				bank_name : {
					required : true,
					chkRegexName: true
				},
				acc_holder_name : {
					required : true,
					chkRegexName : true
				},
				sort_code11 : {
					required : true,
					chkRegexNumber:true
				},
				sort_code21 : {
					required : true,
					chkRegexNumber:true
				},
				sort_code31 : {
					required : true,
					chkRegexNumber:true
				},
				acc_number : {
					required : true,
					number : true
				},
				percentages : {
					required : true,
					number : true
				}
			},

			// Messages for form validation
			messages : {
				org_name : {
					required : 'Please enter organisation name',
					chkRegexName : 'Please enter valid organisation name'
				},
				trustee : {
					required : 'Please enter trustee name',
					chkRegexName : 'Please enter valid trustee name'
				},
				phone_number : {
					required : 'Please enter phone number',
					chkRegexNumber: 'Please enter a VALID number'
				},
				reg_date : {
					required : 'Please select registration date'
				},
				address : {
					required : 'Please enter your full address'
				},
				org_type : {
					required : 'Please select type'
				},
				percentage : {
					required : 'Please enter percentage'
				},
				bank_name : {
					required : 'Please enter bank name',
					chkRegexName : 'Please enter valid bank name'
				},
				acc_holder_name : {
					required : 'Please enter name',
					chkRegexName : 'Please enter valid account holder name'
				},
				sort_code11 : {
					required : 'Please enter sort code',
					chkRegexNumber : 'Please enter valid number'
				},
				sort_code21 : {
					required : 'Please enter sort code',
					chkRegexNumber : 'Please enter valid number'
				},
				sort_code31 : {
					required : 'Please enter sort code',
					chkRegexNumber : 'Please enter valid number'
				},
				acc_number : {
					required : 'Please enter account number',
					number : 'Please enter valid number'
				},
				percentages : {
					required : 'Please enter percentage',
					number : 'Please enter valid number'
				}
			},

			// Do not change code below
			errorPlacement : function(error, element) {
				error.insertAfter(element.parent());
			}
			 
		});
		

$('#org_name').keydown(function (e) {
if (e.ctrlKey || e.altKey) {
e.preventDefault();
} else {
var key = e.keyCode;
if (!((key == 9) || (key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {
e.preventDefault();
}
}
});

		
 $("#phone_number").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 9 && e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
        //$("#errmsg").html("Digits Only").show().fadeOut("slow");
               return false;
    }
   });	
   
$('#trustee').keydown(function (e) {
if (e.ctrlKey || e.altKey) {
e.preventDefault();
} else {
var key = e.keyCode;
if (!((key == 9) || (key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {
e.preventDefault();
}
}
});   	
$('#bank_name').keydown(function (e) {
if (e.ctrlKey || e.altKey) {
e.preventDefault();
} else {
var key = e.keyCode;
if (!((key == 9) || (key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {
e.preventDefault();
}
}
});   
$('#acc_holder_name').keydown(function (e) {
if (e.ctrlKey || e.altKey) {
e.preventDefault();
} else {
var key = e.keyCode;
if (!((key == 9) || (key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {
e.preventDefault();
}
}
});

 $("#sort_code1").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 9 && e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
        //$("#errmsg").html("Digits Only").show().fadeOut("slow");
               return false;
    }
   });

 $("#sort_code2").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 9 && e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
        //$("#errmsg").html("Digits Only").show().fadeOut("slow");
               return false;
    }
   }); 
 $("#sort_code3").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 9 && e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
        //$("#errmsg").html("Digits Only").show().fadeOut("slow");
               return false;
    }
   }); 
$("#acc_number").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 9 && e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
        //$("#errmsg").html("Digits Only").show().fadeOut("slow");
               return false;
    }
   });     		
		
$("#sort_code11").bind('keyup', function(event) {

	if(isNaN($('#sort_code1').val())) {
      alert("Please enter Valid number!");
	  $('#sort_code1').val('');
      e.preventDefault();
	  	  
	}
	 } );
	 $("#sort_code21").bind('keyup', function(event) {

	if(isNaN($('#sort_code2').val())) {
      alert("Please enter Valid number!");
	  $('#sort_code2').val('');
      e.preventDefault();
	  	  
	}
	 
	 } );
	 $("#sort_code31").bind('keyup', function(event) {

	if(isNaN($('#sort_code3').val())) {
      alert("Please enter Valid number!");
	  $('#sort_code3').val('');
      e.preventDefault();
	  	  
	}
	 
	 } );	
	 
$("#percentage").bind('keyup', function(event) {

if(isNaN($(this).val())) {
      alert("Please enter Valid number!");
	  $(this).val('');
      e.preventDefault();
	  	  
	}
    var maxKPIPercentage = 100;
    var totalKPIPercentage = 0;

        if ($.trim($(this).val()).length > 0) {
            var ctrlVaL = $.trim($(this).val()).replace(/[%,]+/g, '');
            totalKPIPercentage = totalKPIPercentage + parseFloat(ctrlVaL);
			if (totalKPIPercentage > parseFloat(maxKPIPercentage))
			{
        	alert("Contribute Percentage Exceeded");
			$(this).val('');
			}
	    event.stopPropagation();
        }

    

});	 	
$("#edit_charity_myform").submit(function(e) { 
	e.preventDefault();
 	var url = "<?php echo base_url(); ?>add_charity/update";  
	 var form = $('#edit_charity_myform');
        var isValid = form.valid();
		
        if (isValid) { //alert('hi');
		if($('#percentage').val() == '') {
			alert('Please enter contribute percentage');
			return false;
		}
		if($('#sort_code1').val() == '') {
			alert('Please enter sort code1');
			return false;
		}
		if($('#sort_code2').val() == '') {
			alert('Please enter sort code2');
			return false;
		}

		if($('#sort_code3').val() == '') {
			alert('Please enter sort code3');
			return false;
		}
		if($('#sort_code1').val().length != 2 || $('#sort_code2').val().length != 2 || $('#sort_code3').val().length != 2) {
			alert('Please enter two digit sort code');
			return false;
		}
			$.ajax({
				   type: "POST",
				   url: url,
	               data: new FormData(this),
				   contentType: false,
        		   cache: false,
		           processData:false,
				   success: function(data)
				   {
					   //alert(data);
					   
					  if(data == 0)
					   {
						   alert('Details updated successfully');
						   window.location = 'trusts';
					  }
					  else
					   alert('Please check your details or error in updating.');
				   }
				 });
		
			return false; // avoid to execute the actual submit of the form.
		}
});

$('input[id^="upload_').on("change",function(e){ 
getId = this.id;

var lastChar = getId.substr(getId.length - 1);
//alert(lastChar);

if($("#doc_type"+lastChar).val() == '') {
    alert("Please select doc type!");
	 $('#'+getId+'_reset').val('');
	reset_form_element( $('#'+getId) );
	return false;
   }

getVal = $("#"+getId).val();
var reg=/(.jpeg|.jpg|.doc|.JPG|.JPEG|.pdf|.docx)$/;
    if (!reg.test(getVal)) {
        alert('Invalid File Type');
		 reset_form_element( $('#'+getId));
        return false;
    }
   
});
function reset_form_element (e) {
    e.wrap('<form>').parent('form').trigger('reset');
    e.unwrap();
}
$('#reg_date').datepicker({
			changeYear:true,
			maxDate: '+0d',
			yearRange: "1901:2020",
			dateFormat : 'mm/dd/yy',
			prevText : '<i class="fa fa-chevron-left"></i>',
			nextText : '<i class="fa fa-chevron-right"></i>',
		});
	
		
$('#edit_close_2').click(function(){ 
		$('#dialog').dialog('close');
		});
		
$('select[id^="doc_type').on("change",function(e){  //alert('came');

	var getId = this.id;
	var lastChar = getId.substr(getId.length - 1);

	//alert(lastChar);
	
	for(var i=lastChar; i>=0; i--)
	{
		//alert(i);
		if(i!=lastChar)
		{
		if($("#doc_type"+i).val() == $("#doc_type"+lastChar).val())
		{
			alert("Please select another doc type!");
			$(this).val(['']);
			return false
		 }
		}
	}
	
	for(var i=lastChar; i<=4; i++)
	{
		//alert(i);
		if(i!=lastChar)
		{
		if($("#doc_type"+i).val() == $("#doc_type"+lastChar).val())
		{
			alert("Please select another doc type!");
			$(this).val(['']);
			return false
		 }
		}
	}

    });		
});		

</script>                    