<div id="main" role="main">

	<!-- MAIN CONTENT -->
	<div id="content" class="container">

		<div class="row">
			<div class="col-lg-12"><br>
			<div class="col-sm-offset-4 col-sm-4">
				<div class="well">
					<form action="login" id="login-form" class="smart-form client-form" method="post">
						<header>
							Talk to Give Charities Portal
						</header>
						<p align="center" style="color:red"><?php if($this->session->flashdata('login_status')!='') echo $this->session->flashdata('login_status');?></p>
						<fieldset>
							
							<section>
								<label class="label">Username</label>
								<label class="input"> 
									<input type="text" name="username" id="username" onkeydown="lettersonly(this.id)">
									</label>
							</section>

							<section>
								<label class="label">Password</label>
								<label class="input"> 
									<input type="password" name="password" id="password">
									<b class="tooltip tooltip-top-right"><i class="fa fa-lock txt-color-teal"></i> Enter your password</b> </label>
								
							</section>

							
						</fieldset>
						<footer>
							<button type="submit" class="btn btn-primary">Login</button>
						</footer>
					</form>

				</div>
				
				
			</div>
			</div>
		</div>
	</div>

</div>

<script type="text/javascript">
function lettersonly(getId) { //alert('sdgs');
		$("#"+getId).keydown(function (e) {
		if (e.ctrlKey || e.altKey) {
		e.preventDefault();
		} else {
		var key = e.keyCode;
		//alert(key);
		if (!( (key == 9) || (key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {
		e.preventDefault();
		}
		}
		});
}
	$(function() {
		// Validation
		$("#login-form").validate({
			// Rules for form validation
			rules : {
				username : {
					required : true,
				},
				password : {
					required : true,
					minlength : 3,
					maxlength : 20
				}
			},

			// Messages for form validation
			messages : {
				username : {
					required : 'Please enter your username',
				},
				password : {
					required : 'Please enter your password'
				}
			},

			// Do not change code below
			errorPlacement : function(error, element) { 
				error.insertAfter(element.parent());
			}
		});
	});
</script>