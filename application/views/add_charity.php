       
<!-- ==========================CONTENT STARTS HERE ========================== -->
<!-- MAIN PANEL -->
<div id="main" role="main">
	<?php
		//configure ribbon (breadcrumbs) array("name"=>"url"), leave url empty if no url
		//$breadcrumbs["New Crumb"] => "http://url.com"
		$breadcrumbs["Misc"] = "";
		//include("inc/ribbon.php");
	?>

	<!-- MAIN CONTENT -->
	<div id="content">

		<!-- row -->
		
		<div class="row">
		
			<div class="col-sm-12">
	                       
				
               <div class="well"> 
		
				<div id="myTabContent1 ">
					<div class="tab-pane fade in active" id="s1">
                    <table width="100%">
                             <tr>
                                   <td width="50%"> <h1> <b><i>Add New Charitable Trust</i></b></h1></td>
                                    <td align="right"> <div id="buttonPlaceholder" align="right"><a href="trusts">Back To List</a> </div></td>
                                
                           	</tr>
                           </table>
						
						<form id="add_charity" action="docpost" name="add_charity" class="smart-form" novalidate="novalidate" method="post" enctype="multipart/form-data">
                        
                   
								  <fieldset>
                                    <legend><br />Generic Info</legend>  <br />
                                    <div class="col col-6">
										<div class="form-group">
											<section >Organisation Name
												<label class="input">
													<input type="text" name="org_name" id="org_name" placeholder="Enter Name" maxlength="50" tabindex="0" autocomplete="off">
												</label>
											</section>
                                            <section>
												<label class="input"> Trustee
													<input type="text" name="trustee"  id="trustee" placeholder="Enter Name" maxlength="30" autocomplete="off">
												</label>
											</section>
                                            <section>
												<label class="input"> Address</label>
													<textarea rows="3" cols="74" name="address"  id="address" placeholder="Enter Address" style="resize:none" ></textarea> 
												
											</section>
                                            <section> Contribution Percentage
											<label class="input">
												<input type="text" name="percentage" id="percentage" placeholder="Enter Number only" maxlength="5" autocomplete="off"> 
											</label>
                                            
										</section>
											
										</div>
                                        </div>

										<div class="col col-6">
										<div class="form-group">
											
                                            <section >Phone Number
												<label class="input"> 
													<input type="text" name="phone_number" id="phone_number" placeholder="Enter Number" maxlength="15" autocomplete="off">
												</label>
											</section>
											<section >Registration Date
                                            <label class="input"> <i class="icon-append fa fa-calendar"></i>
													<input type="text" name="reg_date" id="reg_date" readonly="readonly" autocomplete="off" >
												</label>
											</section> 
                                            
											<section >Type
												<label class="radio">
													<input type="radio" name="org_type" checked="" value="1">
													<i></i>Non Profit Organisation</label> 
												<label class="radio">
													<input type="radio" name="org_type" value="2">
													<i></i>Charity</label>
												<label class="radio">
													<input type="radio" name="org_type" value="3">
													<i></i>Other</label>
											</section>
										</div> 
                                        </div> 
                                         
									 </fieldset>
                                     
                                     <fieldset>
                                    <legend><br />Finance  Info</legend> <br />

										<div class="row">
											<section class="col col-6">Bank Name
                                            <label class="input">
												<input type="text" name="bank_name" id="bank_name" placeholder="Enter Name" maxlength="30" autocomplete="off">
											</label>
											</section>
										</div>
										<div class="row">
										<section class="col col-6"> Account Holder Name
											<label class="input">
												<input type="text" name="acc_holder_name" id="acc_holder_name" placeholder="Enter Name" maxlength="30" autocomplete="off"> 
											</label>
                                            
										</section>
                                        </div>
										<div class="row">
										<section class="col-sm-12 m-l-20"> Sort Code
											<label class="input"> 
                                            <div class="col-sm-3   m-r-10"><input type="text" name="sort_code1" id="sort_code1" maxlength="2" placeholder="Sort Code1" autocomplete="off"></div>										
											<div class="col-sm-3"><input maxlength="2" type="text" name="sort_code2" id="sort_code2" placeholder="Sort Code2" autocomplete="off"></div>	
											
											<div class="col col-3"><input type="text" name="sort_code3" id="sort_code3" placeholder="Sort Code3" maxlength="2" autocomplete="off"> </div>										
											</label>
										</section>
                                        </div>
                                        <div class="row">
										<section class="col col-6"> Account Number
											<label class="input">
												<input type="text" name="acc_number" id="acc_number" placeholder="Enter Number" maxlength="20" autocomplete="off">
											</label>
                                            
										</section>
                                        </div>
					 <!-- Upload Documents -->
			                       <legend><br />Charities Login Info</legend>
                                   <br />
                     					<div class="row">
										<section class="col col-6"> Username
											<label class="input">
												<input type="text" name="charity_username" id="charity_username" placeholder="Enter Name " maxlength="20" autocomplete="off" >
											</label>
                                            
										</section>
                                        </div>
                                        <div class="row">
										<section class="col col-6"> Password
											<label class="input">
												<input type="password" name="charity_password" id="charity_password" placeholder="Enter Password" maxlength="20" autocomplete="off" >
											</label>
                                            
										</section>
                                        </div>
                                        <div class="row">
										<section class="col col-6"> Email
											<label class="input">
												<input type="text" name="charity_email" id="charity_email" placeholder="Enter Email" maxlength="60" autocomplete="off" >
											</label>
                                            
										</section>
                                        </div>
                     			
                                    </fieldset>
                                    <fieldset>
                                    <legend><br />Upload Documents</legend>
                                    <br />
                                    
                                    <div class="form-group">
                                    
                                    
                                    
                                    <div id="fileinput0">
										<div class="row">
                                            <section class="col-sm-3"> 
                                                <label class="pull-left"> Select doc type</label>
                                                <div  class="col col-8"><select name="doc_type0" class="form-control" id="doc_type0" class="hello">
                                                            <option value="" >Select</option>
                                                            <option value="ID Proof">ID Proof</option>
                                                            <option value="Address Proof">Address Proof</option>
                                                            <option value="Stamp Pager doc">Stamp Pager doc</option>
                                                            <option value="Signed Authority Letter">Signed Authority Letter</option>
                                                            <option value="Bank Letter">Bank Letter</option>
                                                        </select> </div>
                                            </section>
                                            
                                            <section class="col-sm-3">
													<div class="input input-file">
														<span class="button">
                                                        	<input type="file"  onchange="this.parentNode.nextSibling.value = this.value" name="upload_0" id="upload_0">Browse</span><input type="text" readonly="" id="upload_0_reset"  style="width:305px;" placeholder="Include some files">
													</div>
											</section>
                                            <section class="col-sm-3">  
                                             
                                                
                                                   
                                                    <div id="showdeleteupload_0" style="display:none;float:left">
                                                        <a  class="btn btn-danger btn-sm" id="delete_upload_0" rel="tooltip" href="javascript: void(0);">
                                                            <i class="fa fa-remove"></i> Delete</a></div>
															
													 <div style="float:left">  | + <a href="javascript: void(0);" id="add_more_1" class="btn btn-default btn-sm" style="text-decoration:underline">
                                                    <em>Add More</em></a></div> 
                                                  
                                              
                                            </section>
                                           <!-- <section class="col-sm-3"> 
                                               
                                            </section> -->
                                        </div>
										</div>
                                        <div id="fileinput1" style="display:none">
										<div class="row">
                                            <section class="col-sm-3"> 
                                                <label class="pull-left"> Select doc type</label>
                                                <div  class="col col-8"><select name="doc_type1" class="form-control" id="doc_type1" class="hello">
                                                            <option value="" >Select</option>
                                                            <option value="ID Proof">ID Proof</option>
                                                            <option value="Address Proof">Address Proof</option>
                                                            <option value="Stamp Pager doc">Stamp Pager doc</option>
                                                            <option value="Signed Authority Letter">Signed Authority Letter</option>
                                                            <option value="Bank Letter">Bank Letter</option>
                                                        </select> </div>
                                            </section>
                                            
                                            <section class="col-sm-3">
													<div class="input input-file">
														<span class="button">
                                                        	<input type="file"  onchange="this.parentNode.nextSibling.value = this.value" name="upload_1" id="upload_1">Browse</span><input type="text" readonly="" id="upload_1_reset"  style="width:305px;" placeholder="Include some files">
													</div>
											</section>
                                            <section class="col-sm-3">  
                                             
                                                
                                                   
                                                    <div id="showdeleteupload_1" style="display:none;float:left">
                                                        <a  class="btn btn-danger btn-sm" id="delete_upload_1" rel="tooltip" href="javascript: void(0);">
                                                            <i class="fa fa-remove"></i> Delete</a></div>
															
													 <div style="float:left">  | + <a href="javascript: void(0);" id="add_more_2" class="btn btn-default btn-sm" style="text-decoration:underline">
                                                    <em>Add More</em></a></div> 
                                                  
                                              
                                            </section>
                                           <!-- <section class="col-sm-3"> 
                                               
                                            </section> -->
                                        </div>
                                        
                                        
                                        
                                        
                                     </div>
                                     <div id="fileinput2" style="display:none">
										<div class="row">
                                            <section class="col-sm-3"> 
                                                <label class="pull-left"> Select doc type</label>
                                                <div  class="col col-8"><select name="doc_type2" class="form-control" id="doc_type2" class="hello">
                                                            <option value="" >Select</option>
                                                            <option value="ID Proof">ID Proof</option>
                                                            <option value="Address Proof">Address Proof</option>
                                                            <option value="Stamp Pager doc">Stamp Pager doc</option>
                                                            <option value="Signed Authority Letter">Signed Authority Letter</option>
                                                            <option value="Bank Letter">Bank Letter</option>
                                                        </select> </div>
                                            </section>
                                            
                                            <section class="col-sm-3">
													<div class="input input-file">
														<span class="button">
                                                        	<input type="file"  onchange="this.parentNode.nextSibling.value = this.value" name="upload_2" id="upload_2">Browse</span><input type="text" readonly="" id="upload_2_reset"  style="width:305px;" placeholder="Include some files">
													</div>
											</section>
                                            <section class="col-sm-3">  
                                             
                                                
                                                   
                                                    <div id="showdeleteupload_2" style="display:none;float:left">
                                                        <a  class="btn btn-danger btn-sm" id="delete_upload_2" rel="tooltip" href="javascript: void(0);">
                                                            <i class="fa fa-remove"></i> Delete</a></div>
															
													 <div style="float:left">  | + <a href="javascript: void(0);" id="add_more_3" class="btn btn-default btn-sm" style="text-decoration:underline">
                                                    <em>Add More</em></a></div> 
                                                  
                                              
                                            </section>
                                           <!-- <section class="col-sm-3"> 
                                               
                                            </section> -->
                                        </div>
                                        
                                        
                                        
                                        
                                     </div>
	                                    <div id="fileinput3" style="display:none">
										<div class="row">
                                            <section class="col-sm-3"> 
                                                <label class="pull-left"> Select doc type</label>
                                                <div  class="col col-8"><select name="doc_type3" class="form-control" id="doc_type3" class="hello">
                                                            <option value="" >Select</option>
                                                            <option value="ID Proof">ID Proof</option>
                                                            <option value="Address Proof">Address Proof</option>
                                                            <option value="Stamp Pager doc">Stamp Pager doc</option>
                                                            <option value="Signed Authority Letter">Signed Authority Letter</option>
                                                            <option value="Bank Letter">Bank Letter</option>
                                                        </select> </div>
                                            </section>
                                            
                                            <section class="col-sm-3">
													<div class="input input-file">
														<span class="button">
                                                        	<input type="file"  onchange="this.parentNode.nextSibling.value = this.value" name="upload_3" id="upload_3">Browse</span><input type="text" readonly="" id="upload_3_reset"  style="width:305px;" placeholder="Include some files">
													</div>
											</section>
                                            <section class="col-sm-3">  
                                             
                                                
                                                   
                                                    <div id="showdeleteupload_3" style="display:none;float:left">
                                                        <a  class="btn btn-danger btn-sm" id="delete_upload_3" rel="tooltip" href="javascript: void(0);">
                                                            <i class="fa fa-remove"></i> Delete</a></div>
															
													 <div style="float:left">  | + <a href="javascript: void(0);" id="add_more_4" class="btn btn-default btn-sm" style="text-decoration:underline">
                                                    <em>Add More</em></a></div> 
                                                  
                                              
                                            </section>
                                           <!-- <section class="col-sm-3"> 
                                               
                                            </section> -->
                                        </div>
                                        
                                        
                                        
                                        
                                     </div>
                                     <div id="fileinput4" style="display:none">
										<div class="row">
                                            <section class="col-sm-3"> 
                                                <label class="pull-left"> Select doc type</label>
                                                <div  class="col col-8"><select name="doc_type4" class="form-control" id="doc_type4" class="hello">
                                                            <option value="" >Select</option>
                                                            <option value="ID Proof">ID Proof</option>
                                                            <option value="Address Proof">Address Proof</option>
                                                            <option value="Stamp Pager doc">Stamp Pager doc</option>
                                                            <option value="Signed Authority Letter">Signed Authority Letter</option>
                                                            <option value="Bank Letter">Bank Letter</option>
                                                        </select> </div>
                                            </section>
                                            
                                            <section class="col-sm-3">
													<div class="input input-file">
														<span class="button">
                                                        	<input type="file"  onchange="this.parentNode.nextSibling.value = this.value" name="upload_4" id="upload_4">Browse</span><input type="text" readonly="" id="upload_4_reset"  style="width:305px;" placeholder="Include some files">
													</div>
											</section>
                                            <section class="col-sm-3">  
                                             
                                                
                                                   
                                                    <div id="showdeleteupload_4" style="display:none;float:left">
                                                        <a  class="btn btn-danger btn-sm" id="delete_upload_4" rel="tooltip" href="javascript: void(0);">
                                                            <i class="fa fa-remove"></i> Delete</a></div>
															
													 <div style="float:left">  | + <a href="javascript: void(0);" id="add_more_5" class="btn btn-default btn-sm" style="text-decoration:underline">
                                                    <em>Add More</em></a></div> 
                                                  
                                              
                                            </section>
                                           <!-- <section class="col-sm-3"> 
                                               
                                            </section> -->
                                        </div>
                                        
                                        
                                        
                                        
                                     </div>
										
                                      
                                     </div>
                                         </fieldset>
                                         
                                   
									 
                        <p>Note:<span style="text-decoration:underline; color:#2c699d">Only the following doc type will be accepted - .doc, .pdf, .jpeg/.jpg</span></p>
									

									<footer>
										
    									<button type="button" class="btn btn-primary" onclick="window.location='trusts';">
											Cancel
										</button>
										<button type="submit" class="btn btn-primary">
											Create
										</button>
									</footer>
								</form>
                                

                         </fieldset>
                        
                    </div>
		
					
				</div>
		
			</div>
		
		</div>
		
		<!-- end row -->
	</div>
	<!-- END MAIN CONTENT -->
</div>
</div>

<div id="vendorID" style="display:none"></div>
<!-- END MAIN PANEL -->
<!-- ==========================CONTENT ENDS HERE ========================== -->
<script src="<?php echo base_url(); ?>/js/plugin/jquery-form/jquery-form.min.js"></script>

<!-- PAGE FOOTER -->
<script type="text/javascript">
$.validator.addMethod("chkRegex", function(value, element) {
        return this.optional(element) || /^[a-z0-9 \\-]+$/i.test(value);
    }, "Username must contain only letters, numbers, or dashes.");

$.validator.addMethod("chkRegexName", function(value, element) {
        return this.optional(element) || /^[a-z \\-]+$/i.test(value);
    }, "Username must contain only letters, numbers, or dashes.");
		
$.validator.addMethod("chkRegexNumber", function(value, element) {
        return this.optional(element) || /^[0-9 \\-]+$/i.test(value);
    }, "Username must contain only letters, numbers, or dashes.");
			
	$(document).ready(function() {

		var $checkoutForm = $('#add_charityss').validate({
		// Rules for form validation
			rules : {
				org_name : {
					required : true,
					chkRegexName:true
				},
				trustee : {
					required : true,
					chkRegexName:true
				},
				phone_number : {
					required : true,
					chkRegexNumber:true
				},
				reg_date : {
					required : true
				},
				address : {
					required : true
				},
				org_type : {
					required : true
				},
				percentages1 :{
					required : true
				},
				bank_name : {
					required : true,
					chkRegexName: true
				},
				acc_holder_name : {
					required : true,
					chkRegexName : true
				},
				sort_code1 : {
					required : true,
				},
				sort_code1 : {
					required : true,
				},
				sort_code3 : {
					required : true,
				},
				acc_number : {
					required : true,
					number : true
				},
				percentage : {
					required : true,
				},
				doc_type0 : {
					required : true
				},
				upload_0 : {
					required : true
				},
			},

			// Messages for form validation
			messages : {
				org_name : {
					required : 'Please enter organisation name',
					chkRegexName : 'Please enter valid organisation name'
				},
				trustee : {
					required : 'Please enter trustee name',
					chkRegexName : 'Please enter valid trustee name'
				},
				phone_number : {
					required : 'Please enter phone number',
					chkRegexNumber: 'Please enter a VALID number'
				},
				reg_date : {
					required : 'Please select registration date'
				},
				address : {
					required : 'Please enter your full address'
				},
				org_type : {
					required : 'Please select type'
				},
				percentagess : {
					required : 'Please enter percentagsddde'
				},
				bank_name : {
					required : 'Please enter bank name',
					chkRegexName : 'Please enter valid bank name'
				},
				acc_holder_name : {
					required : 'Please enter name',
					chkRegexName : 'Please enter valid trustee name'
				},
				sort_code11 : {
					required : 'Please enter sort code',
					chkRegexNumber: 'Please enter valid number'
				},
				sort_code21 : {
					required : 'Please enter sort code',
					chkRegexNumber : 'Please enter valid number'
				},
				sort_code31 : {
					required : 'Please enter sort code',
					chkRegexNumber: 'Please enter valid number'
				},
				acc_number : {
					required : 'Please enter account number',
					number : 'Please enter valid number'
				},
				percentage1 : {
					required : 'Please enter percentage',
					number : 'Please enter valid number'
				},
				doc_type0 : {
					required : 'Please select doc type'
				},
				upload_0 : {
					required : 'Please select doc'
				},
			},

			// Do not change code below
			errorPlacement : function(error, element) {
				error.insertAfter(element.parent());
			}
			 
		});

$('#org_name').keydown(function (e) {
if (e.ctrlKey || e.altKey) {
e.preventDefault();
} else {
var key = e.keyCode;
//alert(key);
if (!( (key == 9) || (key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {
e.preventDefault();
}
}
});

		
 $("#phone_number").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 9 && e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
        //$("#errmsg").html("Digits Only").show().fadeOut("slow");
               return false;
    }
   });	
   
$('#trustee').keydown(function (e) {
if (e.ctrlKey || e.altKey) {
e.preventDefault();
} else {
var key = e.keyCode;
if (!((key == 9) || (key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {
e.preventDefault();
}
}
});   	
$('#bank_name,#charity_username').keydown(function (e) {
if (e.ctrlKey || e.altKey) {
e.preventDefault();
} else {
var key = e.keyCode;
if (!((key == 9) || (key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {
e.preventDefault();
}
}
});   
$('#acc_holder_name').keydown(function (e) {
if (e.ctrlKey || e.altKey) {
e.preventDefault();
} else {
var key = e.keyCode;
if (!((key == 9) || (key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {
e.preventDefault();
}
}
});

 $("#sort_code1").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 9 && e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
        //$("#errmsg").html("Digits Only").show().fadeOut("slow");
               return false;
    }
   });

 $("#sort_code2").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 9 && e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
        //$("#errmsg").html("Digits Only").show().fadeOut("slow");
               return false;
    }
   }); 
 $("#sort_code3").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 9 && e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
        //$("#errmsg").html("Digits Only").show().fadeOut("slow");
               return false;
    }
   }); 
$("#acc_number").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 9 && e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
        //$("#errmsg").html("Digits Only").show().fadeOut("slow");
               return false;
    }
   });         

$("#sort_code11").bind('keyup', function(event) {

	if(isNaN($('#sort_code1').val())) {
      alert("Please enter Valid number!");
	  $('#sort_code1').val('');
      e.preventDefault();
	  	  
	}
	 } );
	 $("#sort_code21").bind('keyup', function(event) {

	if(isNaN($('#sort_code2').val())) {
      alert("Please enter Valid number!");
	  $('#sort_code2').val('');
      e.preventDefault();
	  	  
	}
	 
	 } );
	 $("#sort_code31").bind('keyup', function(event) {

	if(isNaN($('#sort_code3').val())) {
      alert("Please enter Valid number!");
	  $('#sort_code3').val('');
      e.preventDefault();
	  	  
	}
	 
	 } );

 $("#percentage").bind('keyup', function(event) {

if(isNaN($(this).val())) {
      alert("Please enter Valid number!");
	  $(this).val('');
      e.preventDefault();
	  	  
	}
    var maxKPIPercentage = 100;
    var totalKPIPercentage = 0;

        if ($.trim($(this).val()).length > 0) {
            var ctrlVaL = $.trim($(this).val()).replace(/[%,]+/g, '');
            totalKPIPercentage = totalKPIPercentage + parseFloat(ctrlVaL);
			if (totalKPIPercentage > parseFloat(maxKPIPercentage))
			{
        	alert("Contribute Percentage Exceeded");
			$(this).val('');
			}
	    	event.stopPropagation();
        }

    

});
		// START AND FINISH DATE
		$('#reg_date').datepicker({
			changeYear:true,
			maxDate: '+0d',
			yearRange: "1901:2020",
			dateFormat : 'mm/dd/yy',
			prevText : '<i class="fa fa-chevron-left"></i>',
			nextText : '<i class="fa fa-chevron-right"></i>',
		});
function ValidateEmail(email) {
        var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        return expr.test(email);
    };		

	$("#add_charity").submit(function(e) {  //alert('hi');
	
		if($('#org_name').val() == '') {
			alert('Please enter organisation name!');
			$('#org_name').focus();
			return false;
		}
		
		if($('#phone_number').val() == '') {
			alert('Please enter phone number!');
			$('#phone_number').focus();			
			return false;
		}
		
		if($('#trustee').val() == '') {
			alert('Please enter trustee!');
			$('#trustee').focus();			
			return false;
		}
	
		if($('#reg_date').val() == '') {
			alert('Please enter registration date!');
			$('#reg_date').focus();			
			return false;
		}		

		if($('#address').val() == '') {
			alert('Please enter address!');
			$('#address').focus();			
			return false;
		}
	
		
		if($('#percentage').val() == '') {
			alert('Please enter contribute percentage!');
			$('#percentage').focus();	
			return false;
		}
		
		if($('#bank_name').val() == '') {
			alert('Please enter bank name!');
			$('#bank_name').focus();			
			return false;
		}
	
		if($('#acc_holder_name').val() == '') {
			alert('Please enter account holder name!');
			$('#acc_holder_name').focus();			
			return false;
		}
		if($('#sort_code1').val() == '') {
			alert('Please enter sort code1!');
			$('#sort_code1').focus();			
			return false;
		}
		if($('#sort_code2').val() == '') {
			alert('Please enter sort code2!');
			$('#sort_code2').focus();			
			return false;
		}

		if($('#sort_code3').val() == '') {
			alert('Please enter sort code3!');
			$('#sort_code3').focus();			
			return false;
		}
		
		if($('#acc_number').val() == '') {
			alert('Please enter account number!');
			$('#acc_number').focus();			
			return false;
		}

		if($('#charity_username').val() == '') {
			alert('Please enter charities username!');
			$('#charity_username').focus();			
			return false;
		}

		if($('#charity_password').val() == '') {
			alert('Please enter charities password!');
			$('#charity_password').focus();			
			return false;
		}

		if($('#charity_email').val() == '') {
			alert('Please enter charities email!');
			$('#charity_email').focus();			
			return false;
		}
		else
		{
			 if (!ValidateEmail($("#charity_email").val())) {
    	        alert("Invalid email address.");
				$('#charity_email').focus();				
				return false;
	    	    }
		}

		if($('#doc_type0').val() == '') {
			alert('Please select doc type!');
			return false;
		}
		if($('#upload_0').val() == '') {
			alert('Please select doc !');
			return false;
		}
	
       
		if($('#sort_code1').val().length != 2 || $('#sort_code2').val().length != 2 || $('#sort_code3').val().length != 2) {
			alert('Please enter two digit sort code');
			return false;
		}
		 var url = "<?php echo base_url(); ?>add_charity";  
		
			$.ajax({
				   type: "POST",
				   url: url,
	               data: new FormData(this),
				   contentType: false,
        		   cache: false,
		           processData:false,
				   success: function(data)
				   {
					   //alert(data); 
					   data = data.split(',');
					   errcode = data[0];
					   errmsg = data[1];
					   if(errcode ==0)
					   {
							   // $('#vendorID').modal('show');
							$('#vendorID').text(data[1]);
							$('#vendorID').dialog('open');
							//alert(data); 
							return false;
					   }
					   else
					   {
						 alert(errmsg);
					   }
				   }
				 });
		
			return false; // avoid to execute the actual submit of the form.
		//}
});
/*
$('#add_more').click(function(){ 
var getLen = $('input[type=file]').length+1;
alert(getLen);
if(getLen<=5)
    $('#fileinput').append('<div id="fileinput"><div class="row"><section class="col col-6"> Select doc type<select name="doc_type[]"  ><option value="" selected="" >Select</option><option value="ID Proof">ID Proof</option><option value="Address Proof">Address Proof</option><option value="Stamp Pager doc">Stamp Pager doc</option><option value="Signed Authority Letter">Signed Authority Letter</option><option value="Bank Letter">Bank Letter</option></select><label class="input"><input type="file" name="upload[]" id="upload_'+ getLen +'" /></label></section></div></div>');
else
 alert('Only upto five documents are allowed to upload!')	
});*/
	
	$.widget("ui.dialog", $.extend({}, $.ui.dialog.prototype, {
				_title : function(title) {
					if (!this.options.title) {
						title.html("&#160;");
					} else {
						title.html(this.options.title);
					}
				}
			}));
			
			
		
			$('#vendorID').dialog({
				autoOpen : false,
				width : 600,
				resizable : false,
				modal : true,
				title : "<div class='widget-header'><h4>Vendor Creation Successful</h4></div>",
				buttons : [{
					html : "OK",
					"class" : "btn btn-danger",
					click : function() {
						$(this).dialog("close");
						window.location.href = "trusts";
					}
				}]
			});	


	
$('a[id^="add_more_"]').click(function(){ //alert('hi');
	var getId = this.id;
	getId = getId.split('_');
	getId = getId[2];
	//alert(getId);
	if(getId<5)
	$("#fileinput"+getId).show();
	else
	 alert('Only upto five documents are allowed to upload!')	
    
});

$('select.hellos').change(function () {
        $('select.hello option').attr('disabled', false);
        $('select.hello').each(function() {
            var val = $(this).find('option:selected').val();
            if (!val) return;
            $('select.hello option').filter(function() {
                return $(this).val() == val;
            }).attr('disabled', 'disabled');
        });
    });
	
$('select[id^="doc_type').on("change",function(e){  //alert('came');

	var getId = this.id;
	var lastChar = getId.substr(getId.length - 1);

	//alert(lastChar);
	
	for(var i=lastChar; i>=0; i--)
	{
		//alert(i);
		if(i!=lastChar)
		{
		if($("#doc_type"+i).val() == $("#doc_type"+lastChar).val())
		{
			alert("Please select another doc type!");
			$(this).val(['']);
			return false
		 }
		}
	}
	
	for(var i=lastChar; i<=4; i++)
	{
		//alert(i);
		if(i!=lastChar)
		{
		if($("#doc_type"+i).val() == $("#doc_type"+lastChar).val())
		{
			alert("Please select another doc type!");
			$(this).val(['']);
			return false
		 }
		}
	}

    });
$('input[id^="upload_').on("change",function(e){ 
getId = this.id;
//alert(this.id);
var lastChar = getId.substr(getId.length - 1);

if($("#doc_type"+lastChar).val() == '') {
    alert("Please select doc type!");
	 $('#'+getId+'_reset').val('');
	reset_form_element( $('#'+getId) );
	return false;
   }

getVal = $("#"+getId).val();
var reg=/(.jpeg|.jpg|.doc|.JPG|.JPEG|.pdf|.docx)$/;
    if (!reg.test(getVal)) {
        alert('Invalid File Type');
		 $('#'+getId+'_reset').val('');
		 reset_form_element( $('#'+getId));
        return false;
    }
	var getDoctype = $("#doc_type"+lastChar).val();
	
	var fileName = '<?php echo $this->session->userdata('username').'_'; ?>'+getDoctype+'_'+e.target.files[0].name;
	uploadFile(fileName,e,getId);
   
});

function reset_form_element (e) {
    e.wrap('<form>').parent('form').trigger('reset');
    e.unwrap();
}

function uploadFile(getfilename,e,getId)
{
    $("#add_charity").ajaxSubmit({
        //dataType: 'json',
        success: function(data, statusText, xhr, wrapper){ //alert(data);
		//alert(getfilename);
		if(data == 0)
		{
			alert("Sorry, file already exists.");
			 $('#'+getId+'_reset').val('');
			reset_form_element( $('#'+getId) );
		    e.preventDefault();
		}
		else if(data == 1)
			alert("Sorry, only doc, pdf, jpeg, jpg documents are allowed");			
		else if(data == 2)
		{
			$('#showdelete'+getId).show();
			$("#delete_"+getId).attr("href", getfilename);
			//$("#"+getId).prop('disabled', true);
			//$('#showdelete'+getId).html('<a href="javascript: void(0);" id="delete'+getId+'">Delete</a>');
		}
		else if(data == 3)
			alert("Sorry, there was an error uploading your doc.");	
		
            //$('#display-image').prop("src","/assets/images/tmp/"+data);
            //update relevent product form fields here
        }
    });
}

$('a[id^="delete_upload_"]').click(function(){ //alert('hi');
//alert(this.href);
var gethrefVal = $(this).attr('href');
	var getId = this.id;
	getId = getId.split('_');
	getId = getId[1]+'_'+getId[2];
	//alert(getId);
	
 var url = "<?php echo base_url(); ?>deletedocpost/delete_individual";  
	 	$.ajax({
				   type: "POST",
				   url: url,
	               data: "delete_doc="+gethrefVal,
				   success: function(data)
				   {
					   //alert(data); 
					   //alert(getId);
					   if(data == 0)
					   {
						   //alert('#'+getId'+_reset');
						   $('#'+getId+'_reset').val('');
						   reset_form_element( $('#'+getId) );
						   $('#showdelete'+getId).hide();
						   $("#fileinput"+getId).hide();
						   $("#"+getId).prop('disabled', false);
						   e.preventDefault();
						}
				   }
				 });

return false;
   
});	
})
	
/*	
$('input[type=file]').on('change', function(e) {
    alert($(this).val().replace(/C:\\fakepath\\/i, ''));  
	 var url = "post.php";  
	 	$.ajax({
				   type: "POST",
				   url: url,
	               data: new FormData(this),
				   contentType: false,
        		   cache: false,
		           processData:false,
				   success: function(data)
				   {
					   alert(data); 
				   }
				 });
	
	});*/
</script>	