<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Charities_form extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 function __Construct(){
			parent::__Construct ();
			$this->load->helper(array('form', 'url'));
			$this->load->library('session');
			if($this->session->userdata('charities_username')=='')  redirect('login');
			$this->load->helper('api');		

	}
	
	public function index()
	{
			//echo $this->input->post('vendor_id'); exit;
			
			$charitylist_apiurl = $this->config->item('getupdate_charity_list').'?Vendor_Id='.$this->input->post('vendor_id') .'&Product_Type=VMUK';
			$postdata = '';
			$getList = apiPost($charitylist_apiurl, $postdata); 			
			$data['list'] = json_decode($getList);

			$data['vendor_id'] = $this->input->post('vendor_id');
			$this->load->view('charities_form',$data);
	}
	function update()
	{
		//echo 'hi';
		//print_r($_POST);
		//print_r($_FILES);		
		
		 //exit;
		
			$vendor_id = $this->input->post('vendor_id');
			$org_name = $this->input->post('org_name');
			$phone_number = $this->input->post('phone_number');
			$trustee = $this->input->post('trustee');
			$reg_date = $this->input->post('reg_date');
			$address = $this->input->post('address');
			$percentage = $this->input->post('percentage');
			
			$org_type = $this->input->post('org_type');
			$bank_name = $this->input->post('bank_name');
			$acc_holder_name = $this->input->post('acc_holder_name');
			$sort_code1 = $this->input->post('sort_code1');
			$sort_code2 = $this->input->post('sort_code2');
			
			$sort_code3 = $this->input->post('sort_code3');
			$acc_number = $this->input->post('acc_number');
			//$doc_type = $this->input->post('doc_type0');
			
			
			$doc_type  = '';
			$doc_type = $this->input->post('doc_type');
			
			for($i=0;$i<5;$i++)
			{
				if($i==0 && $this->input->post('doc_type'.$i))
				$doc_type .= ','.$this->input->post('doc_type'.$i);
				elseif($i>0)
				{
				if($this->input->post('doc_type'.$i)!='')
				$doc_type .= ','.$this->input->post('doc_type'.$i);
				else
				break;
				}
				}
			$upload_doc  = '';
			$upload_doc = $this->input->post('upload_doc');
			for($i=0;$i<5;$i++)
			{
				if($i==0 && $this->input->post('doc_type'.$i))
				$upload_doc .= ','.$this->session->userdata('username').'_'.$this->input->post('doc_type'.$i).'_'.$_FILES["upload_".$i]["name"];
				elseif($i>0 && $this->input->post('doc_type'.$i))
				{
				if(isset($_FILES["upload_".$i]["name"]))
				$upload_doc .= ','.$this->session->userdata('username').'_'.$this->input->post('doc_type'.$i).'_'.$_FILES["upload_".$i]["name"];
				else
				break;
				}
				}	
				
				$upload_doc =rtrim($upload_doc,',');
				$upload_doc =ltrim($upload_doc,',');
				$doc_type =rtrim($doc_type,',');
				$doc_type =ltrim($doc_type,',');
				
				//echo $doc_type;
				//echo $upload_doc; exit;
				//exit;
			$target_dir =  'uploaded_file/';
			
			$total_upload = 5;
			$output = '';
			//print_r($_FILES);
			//print_r($_POST);
			
			for($i=0;$i<$total_upload;$i++)
			{
			if(isset($_FILES["upload_".$i]["name"])) {
				
			$_FILES["upload_".$i]["name"] = $this->session->userdata('username').'_'.$this->input->post('doc_type'.$i).'_'.$_FILES["upload_".$i]["name"];
			$input_files[$i] = basename($_FILES["upload_".$i]["name"]);
			$target_file = $target_dir . basename($_FILES["upload_".$i]["name"]);
			$uploadOk = 1;
			$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
			//if (file_exists($target_file)) {
			//$output = "Sorry, file already exists.";
			//$output = 0;
			//$uploadOk = 0; //exit;
			//}
			if($imageFileType != "jpg" && $imageFileType != "doc" && $imageFileType != "docx" && $imageFileType != "jpeg" && $imageFileType != "pdf" ) {
			 //$output = "Sorry, only doc, pdf, jpeg, jpg documents are allowed";
			$output = 1;
			$uploadOk = 0; //exit;
			}
			
			if ($uploadOk == 0) {
			//$output .= "Sorry, your file was not uploaded.";
			} else {
			if (move_uploaded_file($_FILES["upload_".$i]["tmp_name"], $target_file)) {
				//echo "The file ". basename( $_FILES["upload"]["name"][$i]). " has been uploaded.";
				 $output = 2;
				 $getFileName = $_FILES['upload_'.$i]['name'];
				
			} else {
				//$output =  "Sorry, there was an error uploading your doc.";
				 $output = 3; //exit;
			}
			}
			}
			}	
			
//			$doc_type = $doc_type0.','.$doc_type1.','.$doc_type2.','.$doc_type3.','.$doc_type4;
//			$upload_doc = $upload_doc1.','.$upload_doc2.','.$upload_doc3.','.$upload_doc4.','.$upload_doc5;			
			//echo $upload_doc;
			//echo $doc_type;
			$data = array('Organisation_Name' => $org_name,'Phone_No' => $phone_number,'Trustee_Name' => $trustee,'Registration_Date' => $reg_date,'Address' => $address,'Contribute_Percent' => $percentage,'Organisation_Type' => $org_type,'Bank_Name' => $bank_name,'Acc_Holder_Name' => $acc_holder_name,'Sort_Code' => $sort_code1. $sort_code2.$sort_code3,'Acc_No' => $acc_number,'Doc_Type' => $doc_type,'Doc_Name' => $upload_doc,'File_Path' => '','User_Id' => $this->session->userdata('userid'),'ErrCode' => '','ErrMsg' => '','Vendor_Id' => $vendor_id);
			
			 $charitylist_apiurl = $this->config->item('add_charity_list');
			
			//print_r($data); 
			//exit;
			//echo 'hi'; exit;
			$getList = apiPost($charitylist_apiurl, $data); 
			$getList = json_decode($getList);
			echo $getList->ErrCode; 
		
		}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */