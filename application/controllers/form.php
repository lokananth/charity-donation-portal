<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Form extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 function __Construct(){
			parent::__Construct ();
			$this->load->helper(array('form', 'url'));
			$this->load->library('session');
			if($this->session->userdata('username')=='')  redirect('login');
			$this->load->helper('api');		

	}
	public function index()
	{
			 
			$charitylist_apiurl = $this->config->item('getupdate_charity_list').'?Vendor_Id='.$this->input->post('vendor_id') .'&Product_Type=VMUK';
			$postdata = '';
			$getList = apiPost($charitylist_apiurl, $postdata); 			
			$data['list'] = json_decode($getList);
			//echo '<pre>';
			//print_r($getList); exit;
			
			//$this->load->view('header');
			$data['vendor_id'] = $this->input->post('vendor_id');
			$this->load->view('form',$data);
	}
	public function getform()
	{
			//echo $this->input->post('vendor_id'); exit;
			
			$charitylist_apiurl = $this->config->item('getupdate_charity_list').'?Vendor_Id='.$this->input->post('vendor_id') .'&Product_Type=VMUK';
			$postdata = '';
			$getList = apiPost($charitylist_apiurl, $postdata); 			
			$data['list'] = json_decode($getList);

			$data['vendor_id'] = $this->input->post('vendor_id');
			$this->load->view('getform',$data);
	}
	public function charities_form()
	{
			//echo $this->input->post('vendor_id'); exit;
			
			$charitylist_apiurl = $this->config->item('getupdate_charity_list').'?Vendor_Id='.$this->input->post('vendor_id') .'&Product_Type=VMUK';
			$postdata = '';
			$getList = apiPost($charitylist_apiurl, $postdata); 			
			$data['list'] = json_decode($getList);

			$data['vendor_id'] = $this->input->post('vendor_id');
			$this->load->view('charities_form',$data);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */