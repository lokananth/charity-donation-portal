<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Docpost extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __Construct(){
			parent::__Construct ();
			$this->load->helper(array('form', 'url'));
			$this->load->library('session');
	}
	public function index()
	{
			//$autoload['helper'] = array('url');
			$target_dir =  'uploaded_file/';
			
			//echo $target_dir; exit;
			
			$total_upload = 5;
			$output = '';
			//print_r($_FILES);
			//print_r($_POST);
			
			for($i=0;$i<$total_upload;$i++)
			{
			if(isset($_FILES["upload_".$i]["name"])) {
				
			$_FILES["upload_".$i]["name"] = $this->session->userdata('username').'_'.$this->input->post('doc_type'.$i).'_'.$_FILES["upload_".$i]["name"];
			$input_files[$i] = basename($_FILES["upload_".$i]["name"]);
			$target_file = $target_dir . basename($_FILES["upload_".$i]["name"]);
			$uploadOk = 1;
			$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
			//if (file_exists($target_file)) {
			//$output = "Sorry, file already exists.";
			//$output = 0;
			//$uploadOk = 0; //exit;
			//}
			if($imageFileType != "jpg" && $imageFileType != "doc" && $imageFileType != "docx" && $imageFileType != "jpeg" && $imageFileType != "pdf" ) {
			 //$output = "Sorry, only doc, pdf, jpeg, jpg documents are allowed";
			$output = 1;
			$uploadOk = 0; //exit;
			}
			
			if ($uploadOk == 0) {
			//$output .= "Sorry, your file was not uploaded.";
			} else {
			if (move_uploaded_file($_FILES["upload_".$i]["tmp_name"], $target_file)) {
				//echo "The file ". basename( $_FILES["upload"]["name"][$i]). " has been uploaded.";
				 $output = 2;
				 $getFileName = $_FILES['upload_'.$i]['name'];
				
			} else {
				//$output =  "Sorry, there was an error uploading your doc.";
				 $output = 3; //exit;
			}
			}
			}
			}
			echo $output;
	
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */