<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Export_excel extends CI_Controller{
 function __Construct(){
			parent::__Construct ();
			$this->load->library('session');
			if($this->session->userdata('username')=='')  redirect('login');	
			$this->load->helper(array('form', 'url'));
			$this->load->helper('api');	
	}

 public function index() {
	
	$charitylist_apiurl = $this->config->item('get_charity_list');
			$data['User_Id'] = $this->session->userdata('userid');
			$getList = apiPost($charitylist_apiurl, $data); 			
			
			
			
			
			$data = json_decode($getList);
			
			//echo '<pre>';
	//print_r($data);
//echo '<pre>';
	//print_r($objData);
			
			
			 //exit;
	
	$this->load->library('excel');
	//echo 'okoko';
	$heading=array('Name','Trustee','Address','Type','Phone Number','Vendor ID','Registration Date','Added_By','Added_On');
	
	//Create a new Object
    $objPHPExcel = new PHPExcel();
    $objPHPExcel->getActiveSheet()->setTitle("Clarity call Information");
    //Loop Heading
    $rowNumberH = 1;
    $colH = 'A';
    foreach($heading as $h){
        $objPHPExcel->getActiveSheet()->setCellValue($colH.$rowNumberH,$h);
        $colH++;    
    }
	
	 //Loop Result
    $totn=count($data);
    $maxrow=$totn+1;
    $nil=$data;
                $row = 2;
        $no = 1;
		$myval = 0;
        foreach($nil as $n){
            //$numnil = (float) str_replace(',','.',$n->nilai);
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$row,$n->Name);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$row,$n->Trustee);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$row,$n->Address);
			if($n->Type == 1) $n->Type = 'Non Profit Organisation'; elseif($n->Type == 2) $n->Type = 'Charity'; elseif($n->Type == 3) $n->Type = 'Other';
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$row,$n->Type);
			$type = PHPExcel_Cell_DataType::TYPE_STRING;
			$objPHPExcel->getActiveSheet()->setCellValueExplicit('E'.$row,$n->Phone_Number, $type);
            $objPHPExcel->getActiveSheet()->setCellValue('F'.$row,$n->Vendor_Id);
            $objPHPExcel->getActiveSheet()->setCellValue('G'.$row,$n->Registration_Date);
//			$objPHPExcel->getActiveSheet()->setCellValue('H'.$row,$n->Added_By);
			$objPHPExcel->getActiveSheet()->setCellValue('H'.$row,$this->session->userdata('username'));
			$objPHPExcel->getActiveSheet()->setCellValue('I'.$row,$n->Added_On);
			$myval++;
			
            //$objPHPExcel->getActiveSheet()->setCellValueExplicit('D'.$row,$n->benar,PHPExcel_Cell_DataType::TYPE_NUMERIC);
            //$objPHPExcel->getActiveSheet()->setCellValueExplicit('E'.$row,$n->salah,PHPExcel_Cell_DataType::TYPE_NUMERIC);
           //$objPHPExcel->getActiveSheet()->getStyle('F'.$row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->setCellValueExplicit('F'.$row,$n->nilai,PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $row++;
            $no++;
        }
	
	
	//Freeze pane
    //$objPHPExcel->getActiveSheet()->freezePane('A2');
    //Cell Style
    $styleArray = array(
        'borders' => array(
            'allborders' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN
            )
        )
    );
	
	
	$objPHPExcel->getActiveSheet()->getStyle('A1:J'.$maxrow)->applyFromArray($styleArray);
    //Save as an Excel BIFF (xls) file
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');
	date_default_timezone_set('Europe/London');
	header("Cache-Control: no-store, no-cache, must-revalidate");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="charity_list_'.$this->session->userdata('username').'.xls"');
    header('Cache-Control: max-age=0');

    $objWriter->save('php://output');
    exit();
	
	
	
 } 
 

	 public function get_transaction_excel() {
	$charitylist_apiurl = $this->config->item('get_transaction_list');
	
			$getList = apiPost($charitylist_apiurl, $data=''); 			
			
			
			
			
			$data = json_decode($getList);
			
			//echo '<pre>';
	//print_r($data);
//echo '<pre>';
	//print_r($objData);
			
			
			// exit;
	
	$this->load->library('excel');
	//echo 'okoko';
	$heading=array('Transaction ID','Transaction Date','Trust Name','Vendor ID','Customer Mobile Number','Customer ICICID Number','Top Up Amount','Contributed Amount'	);
	
	//Create a new Object
    $objPHPExcel = new PHPExcel();
    $objPHPExcel->getActiveSheet()->setTitle("Clarity call Information");
    //Loop Heading
    $rowNumberH = 1;
    $colH = 'A';
    foreach($heading as $h){
        $objPHPExcel->getActiveSheet()->setCellValue($colH.$rowNumberH,$h);
        $colH++;    
    }
	
	 //Loop Result
    $totn=count($data);
    $maxrow=$totn+1;
    $nil=$data;
                $row = 2;
        $no = 1;
		$myval = 0;
        foreach($nil as $n){
            //$numnil = (float) str_replace(',','.',$n->nilai);
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$row,$n->Transaction_Id);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$row,str_replace('T',' ',$n->Transaction_Date));
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$row,$n->Trust_Name);
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$row,$n->Vendor_Id);
			$type = PHPExcel_Cell_DataType::TYPE_STRING;
			$objPHPExcel->getActiveSheet()->setCellValueExplicit('E'.$row,$n->Customer_Mobile_Number, $type);
            $objPHPExcel->getActiveSheet()->setCellValue('F'.$row,$n->Customer_ICCID_Number);
            $objPHPExcel->getActiveSheet()->setCellValue('G'.$row,$n->Top_Up_Amount);
			$objPHPExcel->getActiveSheet()->setCellValue('H'.$row,$n->Contributed_Amount);
            $row++;
            $no++;
        }
	
	
	//Freeze pane
    //$objPHPExcel->getActiveSheet()->freezePane('A2');
    //Cell Style
    $styleArray = array(
        'borders' => array(
            'allborders' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN
            )
        )
    );
	
	
	$objPHPExcel->getActiveSheet()->getStyle('A1:J'.$maxrow)->applyFromArray($styleArray);
    //Save as an Excel BIFF (xls) file
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');
	date_default_timezone_set('Europe/London');
	header("Expires: 0");
	header("Cache-Control: no-store, no-cache, must-revalidate");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="transaction_list_'.$this->session->userdata('username').'.xls"');
    header('Cache-Control: max-age=0');

    $objWriter->save('php://output');
    exit();
	
	
	 }
 
 
}
?>