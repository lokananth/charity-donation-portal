<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Charities_transactions extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __Construct(){
			parent::__Construct ();
			$this->load->helper(array('form', 'url'));
			$this->load->library('session');
			if($this->session->userdata('charities_username')=='')  redirect('charities_login');	
			$this->load->helper('api');	
	}
	public function index()
	{
		
		if($this->input->post())
		{
			//echo 'hi'; exit;
			//redirect('trusts');		
			//$data['Vendor_Id'] = $this->session->userdata('charities_userid');
			$data['Vendor_Id'] = $this->session->userdata('charities_vendor_id');
			$data['From_Date'] = urlencode($this->input->post('start_date'));
			$data['To_Date'] = urlencode($this->input->post('end_date'));
			$charitylist_apiurl = $this->config->item('get_search_transaction_bydate');  
			//$data = '';
			$getList = apiPost($charitylist_apiurl, $data); 			
			
			//echo '<pre>';
			//print_r($getList); exit;
			
			$data['list'] = json_decode($getList);
			//print_r($data); exit;
			$data['page_title'] = 'Charities Transactions - Charity Donation Portal';
			$this->load->view('charities_header',$data);
			$this->load->view('charities_transactions');
			$this->load->view('footer');	
			
		}
		else
		{
			$transactionlist_apiurl = $this->config->item('get_transaction_list');
	
			$getList = apiPost($transactionlist_apiurl, $data=''); 			
			
			//echo '<pre>';
			//print_r($getList); exit;
			$data['list'] = json_decode($getList);
			$data['page_title'] = 'Charities Transactions - Charity Donation Portal';
			$this->load->view('charities_header',$data);
			$this->load->view('charities_transactions');
			$this->load->view('footer');		
		}
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */