<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Deletedocpost extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __Construct(){
			parent::__Construct ();
			$this->load->helper(array('form', 'url'));
			$this->load->library('session');
			if($this->session->userdata('username')=='')  redirect('login');	
			$this->load->helper('api');	
	}
	public function index()
	{
		
			//print_r($_POST);
			$vendor_id = $this->input->post('vendor_id');
			$org_name = $this->input->post('org_name');
			$phone_number = $this->input->post('phone_number');
			$trustee = $this->input->post('trustee');
			$reg_date = $this->input->post('reg_date');
			$address = $this->input->post('address');
			$percentage = $this->input->post('percentage');
			
			$org_type = $this->input->post('org_type');
			$bank_name = $this->input->post('bank_name');
			$acc_holder_name = $this->input->post('acc_holder_name');
			$sort_code1 = $this->input->post('sort_code1');
			$sort_code2 = $this->input->post('sort_code2');
			
			$sort_code3 = $this->input->post('sort_code3');
			$acc_number = $this->input->post('acc_number');

			$doc_type = ltrim($this->input->post('doc_type'),',');
			$doc_type = rtrim($doc_type,',');
			$upload_doc = ltrim($this->input->post('upload_doc'),',');
			$upload_doc = rtrim($upload_doc,',');
			//$doc_type = 'ID Proof,Address Proof';
			//$upload_doc = 'ID_proof.jpg,Address_proof.jpg';
			$data = array('Organisation_Name' => $org_name,'Phone_No' => $phone_number,'Trustee_Name' => $trustee,'Registration_Date' => $reg_date,'Address' => $address,'Contribute_Percent' => $percentage,'Organisation_Type' => $org_type,'Bank_Name' => $bank_name,'Acc_Holder_Name' => $acc_holder_name,'Sort_Code' => $sort_code1. $sort_code2.$sort_code3,'Acc_No' => $acc_number,'Doc_Type' => $doc_type,'Doc_Name' => $upload_doc,'File_Path' => '','User_Id' => $this->session->userdata('userid'),'ErrCode' => '','ErrMsg' => '','Vendor_Id' => $vendor_id);
			
			 $charitylist_apiurl = $this->config->item('add_charity_list');
			
			//print_r($data); 
			//exit;
			//echo 'hi'; exit;
			$getList = apiPost($charitylist_apiurl, $data); 
			$getList = json_decode($getList);
			//print_r($getList); exit;
			
			echo $getList->ErrCode;
			
			
		//if(unlink('uploaded_file/'.$_POST['delete_doc']))
		//echo '0';
	}
	public function delete_individual()
	{
		if(unlink('uploaded_file/'.$_POST['delete_doc']))
		echo '0';
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */