<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Send_mail extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __Construct(){
			parent::__Construct ();
			$this->load->helper(array('form', 'url'));
			$this->load->helper('api');	
	}
	public function index()
	{
		
		
			//Email to charities
			$this->load->library('email');

			 $config = array (
                  'mailtype' => 'html',
                  'charset'  => 'utf-8',
                  'priority' => '1'
                   );

			$this->email->initialize($config);
			

			$this->email->from('support@charityportal.com', 'Support');
			$this->email->to('p.ramachandran@mundio.com'); 
			//$this->email->cc('another@another-example.com'); 
			//$this->email->bcc('p.ramachandran@mundio.com'); 
			$data['charity_username'] = 'test_charity';
			$data['charity_password'] = 'test_charity';
						
			$html_email = $this->load->view('html_email_message', $data, true);
			$this->email->subject('Charity Login Info');
			$this->email->message($html_email);
			if($this->email->send())
			echo '0';
			else
			echo 1;
			echo $this->email->print_debugger(); exit;
			
		
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */