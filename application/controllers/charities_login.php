<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Charities_login extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 function __Construct(){
			parent::__Construct ();
			$this->load->helper(array('form', 'url'));
			$this->load->library('session');
			$this->load->helper('api');	
			
	}
	public function index()
	{
		
		if($this->input->post())
		{
			$charitylist_apiurl = $this->config->item('get_charity_login');
			$data['UserName'] = $this->input->post('username');
			$data['Password'] = $this->input->post('password');
			$data['ErrCode'] = '';
			$data['ErrMsg'] = '';
			
			$getList = apiPost($charitylist_apiurl, $data); 			
			$getList = json_decode($getList);
			//print_r($getList); exit;
			if($getList->ErrCode == 0)
			{
				$newdata = array('charities_username'  => $this->input->post('username'),'charities_userid'  => $getList->UserId);
				$this->session->set_userdata($newdata);
				redirect('charities_trusts');	
			}
			else
			{
				$this->session->set_flashdata('login_status', 'Login Failed!');
				redirect('charities_login');	
			}
		}
		else
		{
			//echo 'hiaffsd'; exit;
			if($this->session->userdata('charities_username')=='') { 
			$data['page_title'] = 'Charities Login - Charities Donation Portal';
			$this->load->view('charities_header',$data);
			$this->load->view('charities_login');
			$this->load->view('footer');		
			}
			else
			redirect('charities_trusts');	
			
		}
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */