<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Trusts extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __Construct(){
			parent::__Construct ();
			$this->load->library('session');
			$this->load->helper(array('form', 'url'));
			if($this->session->userdata('username')=='')  redirect('login');	
			$this->load->helper('api');	
	}
	public function index()
	{
		
		if($this->input->post())
		{
			redirect('trusts');		
		}
		else
		{

			$charitylist_apiurl = $this->config->item('get_charity_list');
			$data['User_Id'] = $this->session->userdata('userid');
			$getList = apiPost($charitylist_apiurl, $data); 			
			
			$data['list'] = json_decode($getList);
			//echo '<pre>';
			//print_r($data); exit;
	
			$data['page_title'] = 'Dashboard - Charity Donation Portal';
			$this->load->view('header',$data);
			$this->load->view('trusts');
			$this->load->view('footer');		
		}
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */