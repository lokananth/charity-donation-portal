<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 function __Construct(){
			parent::__Construct ();
			$this->load->helper(array('form', 'url'));
			$this->load->library('session');
			$this->load->helper('api');	
			
	}
	public function index()
	{
		
		if($this->input->post())
		{
			//echo 'hi'; exit;
			

			/*$searchType = urlencode('Transaction ID');
			$charitylist_apiurl = $this->config->item('get_search_transaction').'?searchType='.$searchType.'&searchValue=10002';  
			$data['Search_Type'] = '';
			$data['Search_Value'] = '';			
			//$data = '';
			$getList = apiPost($charitylist_apiurl, $data); 			
			
			echo '<pre>';
			print_r($getList); exit;

			
			$charitylist_apiurl = $this->config->item('get_transaction_list');
			$data['Search_Type'] = '';
			$data['Search_Value'] = '';			
			
			$getList = apiPost($charitylist_apiurl, $data); 			
			
			echo '<pre>';
			print_r($getList); exit;
			
			$charitylist_apiurl = $this->config->item('getupdate_charity_list').'?Vendor_Id=1001';
			$data['Vendor_Id'] = '';
			
			
			$getList = apiPost($charitylist_apiurl, $data); 			
			
			echo '<pre>';
			print_r($getList); exit;*/
			
			$charitylist_apiurl = $this->config->item('get_charity_login');
			
			$data['UserName'] = $this->input->post('username');
			$data['Password'] = $this->input->post('password');
			$data['ErrCode'] = '';
			$data['ErrMsg'] = '';
			
			$getList = apiPost($charitylist_apiurl, $data); 			
			$getList = json_decode($getList);
			//print_r($getList); exit;
			if($getList->ErrCode == 0)
			{
			if($getList->User_Role!='A')
			{
					$newdata = array('charities_username'  => $this->input->post('username'),'charities_userid'  => $getList->UserId);
 					$this->session->set_userdata($newdata);
					redirect('charities_trusts');	
				}
			else
			{
				$newdata = array('username'  => $this->input->post('username'),'userid'  => $getList->UserId);
				$this->session->set_userdata($newdata);
				redirect('trusts');	
			}	
			}
			else
			{
				$this->session->set_flashdata('login_status', 'Login Failed!');
				redirect('login');	
			}
		}
		else
		{
			//echo 'hiaffsd'; exit;
			if($this->session->userdata('username')=='') { 
			$data['page_title'] = 'Login - Charity Donation Portal';
			$this->load->view('header',$data);
			$this->load->view('login');
			$this->load->view('footer');		
			}
			else
			redirect('trusts');	
			
		}
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */