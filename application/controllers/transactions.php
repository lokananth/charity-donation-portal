<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Transactions extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __Construct(){
			parent::__Construct ();
			$this->load->helper(array('form', 'url'));
			$this->load->library('session');
			if($this->session->userdata('username')=='')  redirect('login');	
			$this->load->helper('api');	
	}
	public function index()
	{
		
		if($this->input->post())
		{
			//echo 'hi'; exit;
			//redirect('trusts');		
			
			$searchType = urlencode($this->input->post('search_type'));
			$search_value = urlencode($this->input->post('search_value'));
			$charitylist_apiurl = $this->config->item('get_search_transaction').'?searchType='.$searchType.'&searchValue='.$search_value;  
			$data['Search_Type'] = '';
			$data['Search_Value'] = '';			
			//$data = '';
			$getList = apiPost($charitylist_apiurl, $data); 			
			
			//echo '<pre>';
			//print_r($getList); exit;
			
			$data['list'] = json_decode($getList);
			$data['page_title'] = 'Dashboard - Charity Donation Portal';
			$this->load->view('header',$data);
			$this->load->view('transactions');
			$this->load->view('footer');	
			
		}
		else
		{
			$transactionlist_apiurl = $this->config->item('get_transaction_list');
	
			$getList = apiPost($transactionlist_apiurl, $data=''); 			
			
			//echo '<pre>';
			//print_r($getList); exit;
			$data['list'] = json_decode($getList);
			$data['page_title'] = 'Dashboard - Charity Donation Portal';
			$this->load->view('header',$data);
			$this->load->view('transactions');
			$this->load->view('footer');		
		}
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */